package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.web.dto.NotificationSettingDTO;

public interface NotificationSettingService {
    NotificationSettingDTO updateNotificationSetting(NotificationSettingDTO notificationSettingDTO);
    NotificationSettingDTO getNotificationSetting(String notificationType);
}
