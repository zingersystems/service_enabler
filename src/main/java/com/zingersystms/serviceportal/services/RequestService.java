package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.web.dto.CommentInRequestDTO;
import com.zingersystms.serviceportal.web.dto.RequestBareDTO;
import com.zingersystms.serviceportal.web.dto.RequestDTO;
import com.zingersystms.serviceportal.web.dto.RequestOnlyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface RequestService {
    Request findById(String uuid);

    RequestDTO create(RequestBareDTO request);

    Page<RequestDTO> findAll(Pageable pageable);

    Page<RequestOnlyDTO> findAllWithParams(Pageable pageable, String statusUuid, long startDate, long endDate);

    Page<RequestOnlyDTO> findAllByStatusUuid(Pageable pageable, String statusUuid);

    RequestDTO update(RequestBareDTO request);

    RequestDTO delete(String uuid);

    Page<CommentInRequestDTO> getCommentsForRequest(String id,Pageable pageable);
}
