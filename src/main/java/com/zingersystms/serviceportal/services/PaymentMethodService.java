package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.web.dto.PaymentMethodDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaymentMethodService {

    PaymentMethodDTO findByUuid(String uuid);

    PaymentMethodDTO create(PaymentMethodDTO paymentMethodDTO);

    PaymentMethodDTO update(PaymentMethodDTO paymentMethodDTO);

    PaymentMethodDTO delete(String uuid);

    Page<PaymentMethodDTO> findAll(Pageable pageable);
}
