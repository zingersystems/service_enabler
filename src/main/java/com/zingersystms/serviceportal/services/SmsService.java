package com.zingersystms.serviceportal.services;

public interface SmsService {

    void sendMessage(String phoneNumber, String message);
}
