package com.zingersystms.serviceportal.services.registration.listenner;


import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.services.VerificationTokenService;
import com.zingersystms.serviceportal.services.registration.OnRegistrationCompleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private VerificationTokenService verificationTokenService;

    private
    @Value("${service-portal.mail.verification.subject}")
    String subject;

    private
    @Value("${service-portal.mail.verification.url}")
    String confirmationBaseUrl;

    private
    @Value("${service-portal.mail.verification.message}")
    String message;

    private
    @Value("${service-portal.mail.verification.denial}")
    String denial;

    private
    @Value("${service-portal.mail.support}")
    String support;

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) {

        final User user = event.getUser();
        final String token = UUID.randomUUID().toString();

        verificationTokenService.createVerificationTokenForUser(user, token);

        final SimpleMailMessage email = constructEmailMessage(user, token);
        mailSender.send(email);
    }

    private final SimpleMailMessage constructEmailMessage(final User user, final String token) {
        final String recipientAddress = user.getEmail();
        final String greeting = "Hey " + user.getUsername();
        final String confirmationUrl = confirmationBaseUrl + "?token=" + token;
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(greeting + " \r\n" + message + " \r\n" + confirmationUrl + " \r\n" + denial + " \r\n");
        email.setFrom(support);
        return email;
    }

}
