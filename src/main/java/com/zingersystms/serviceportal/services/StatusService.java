package com.zingersystms.serviceportal.services;


import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.web.dto.StatusDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface StatusService {

    Status findById(String uuid);

    Status findByName(String name);

    StatusDTO create(StatusDTO status);

    Page<StatusDTO> findAll(Pageable pageable);

    StatusDTO update(StatusDTO status);

    StatusDTO delete(String uuid);

    StatusDTO setDefaultStatus(String uuid);
}
