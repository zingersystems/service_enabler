package com.zingersystms.serviceportal.services.reqprocessing.listener;


import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.persistence.repository.CommentRepository;
import com.zingersystms.serviceportal.persistence.repository.RequestRepository;
import com.zingersystms.serviceportal.persistence.repository.UserRepository;
import com.zingersystms.serviceportal.services.reqprocessing.OnRequestStatusChangedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class RequestStatusChangedListener implements ApplicationListener<OnRequestStatusChangedEvent> {

    private final JavaMailSender mailSender;

    private
    @Value("${service-portal.mail.support}")
    String support;

    @Resource
    private RequestRepository requestRepository;

    @Resource
    private CommentRepository commentRepository;

    @Resource
    private UserRepository userRepository;

    @Autowired
    public RequestStatusChangedListener(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnRequestStatusChangedEvent event) {
        if(event.getRequest().getCreatedBy().getUserPreference().isEmailNotification()) {
            sendMail(constructMailMessage(event.getRequest()));
        }
        sendCommentNotificationToUser(event.getRequest());
    }

    @Async
    public void sendMail(SimpleMailMessage simpleMailMessage) {
        this.mailSender.send(simpleMailMessage);
    }

    private SimpleMailMessage constructMailMessage(Request request){

        String message = "Hello " + request.getCreatedBy().getFirstName() + ","
                +"\n \nThe status of your request for the service \""
                + request.getServiceId().getName() + "\", created on "
                + request.getCreatedDate() + " has been updated. \nThe request is now in the \""
                + request.getStatusId().getName() + "\" state. \n\n"
                + "Thank you for using our services.";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(request.getCreatedBy().getEmail());
        email.setSubject("Request Status Changed");
        email.setText(message);
        email.setFrom(support);
        return email;
    }

    private void sendCommentNotificationToUser(Request request) {
        Comment comment = createStatusNotificationComment(request);
        this.commentRepository.save(comment);
    }

    private Comment createStatusNotificationComment(Request request) {
        Status previousStatus = this.requestRepository.findLastChangeRevision(request.getId()).getEntity().getStatusId();
        Comment comment = new Comment();
        comment.setCreatedBy(this.userRepository.findByUsernameIgnoreCaseAndDeletedFalse("test"));
        comment.setRequest(request);
        String message = "The status of your request has been updated from \""
                + previousStatus.getName() + "\" to \""
                + request.getStatusId().getName() + "\"";
        comment.setContent(message);
        return comment;
    }
}
