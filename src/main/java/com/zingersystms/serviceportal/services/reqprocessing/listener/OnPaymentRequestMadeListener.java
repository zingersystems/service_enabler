package com.zingersystms.serviceportal.services.reqprocessing.listener;

import com.zingersystms.serviceportal.persistence.model.NotificationType;
import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import com.zingersystms.serviceportal.persistence.repository.NotificationSettingRepository;
import com.zingersystms.serviceportal.services.reqprocessing.OnPaymentRequestMadeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author kkyb on 8/25/17.
 */
@Component
public class OnPaymentRequestMadeListener implements ApplicationListener<OnPaymentRequestMadeEvent> {

    private
    @Value("${service-portal.mail.support}")
    String support;

    private final JavaMailSender mailSender;

    @Resource
    private NotificationSettingRepository notificationSettingRepository;

    @Autowired
    public OnPaymentRequestMadeListener(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnPaymentRequestMadeEvent event) {
        if(event.getPaymentRequest().getRequest().getCreatedBy().getUserPreference().isEmailNotification() &&
                notificationSettingRepository.findByNotificationType(NotificationType.ON_PAYMENT_REQUESTED)
                        .isActivated()) {
            this.mailSender.send(constructMailMessage(event.getPaymentRequest()));
        }
    }

    private SimpleMailMessage constructMailMessage(PaymentRequest paymentRequest) {
        String message = "Hello " + paymentRequest.getRequest().getCreatedBy() + ","
                +"\n \nA payment has been requested for the service \""
                + paymentRequest.getRequest().getServiceId().getName() + "\" "
                + "The details of the payment request are: \n\n"
                + "Payment Request ID: " + paymentRequest.getUuid() + "\n"
                + "Amount: " + paymentRequest.getAmount() + "\n"
                + "Payment Request Motif: " + paymentRequest.getMotif() + "\n\n"
                + "Service ID: " + paymentRequest.getRequest().getServiceId().getUuid() + "\n\n"
                + "Service Request ID: " + paymentRequest.getRequest().getUuid() + "\n"
                + "Service Request Description: " + paymentRequest.getRequest().getDescription()+ "\n";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(paymentRequest.getRequest().getCreatedBy().getEmail());
        email.setSubject("Payment Request");
        email.setText(message);
        email.setFrom(support);
        return email;
    }
}
