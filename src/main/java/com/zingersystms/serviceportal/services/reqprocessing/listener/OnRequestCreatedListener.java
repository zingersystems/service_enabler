package com.zingersystms.serviceportal.services.reqprocessing.listener;

import com.zingersystms.serviceportal.persistence.model.NotificationType;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.NotificationSettingRepository;
import com.zingersystms.serviceportal.persistence.repository.UserRepository;
import com.zingersystms.serviceportal.services.reqprocessing.OnRequestCreatedEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class OnRequestCreatedListener implements ApplicationListener<OnRequestCreatedEvent> {

    @Resource
    private UserRepository userRepository;

    @Resource
    private NotificationSettingRepository notificationSettingRepository;

    @Resource
    private JavaMailSender mailSender;

    private
    @Value("${service-portal.mail.support}")
    String support;

    @Override
    public void onApplicationEvent(OnRequestCreatedEvent event) {
        if(event.getRequest().getAgentId().getUserPreference().isEmailNotification() &&
                this.notificationSettingRepository.findByNotificationType(NotificationType.ON_REQUEST_CREATED)
                        .isActivated()) {
            this.mailSender.send(constructMailMessage(event.getRequest()));
        }
    }

    private SimpleMailMessage constructMailMessage(Request request){

        String message = "Hello Admin,"
                +"\n \nThe user " + request.getCreatedBy() + " requested for the service \""
                + request.getServiceId().getName() + "\" on "
                + request.getCreatedDate() + "."
                + "\nThe description of the request is:\n"
                + "\"" + request.getDescription() + "\"";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(request.getAgentId().getEmail());
        email.setSubject("New Request");
        email.setText(message);
        email.setFrom(support);
        return email;
    }
}
