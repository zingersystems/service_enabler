package com.zingersystms.serviceportal.services.reqprocessing;

import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import org.springframework.context.ApplicationEvent;

public class OnPaymentRequestMadeEvent extends ApplicationEvent {

    private PaymentRequest paymentRequest;

    public OnPaymentRequestMadeEvent(final PaymentRequest paymentRequest) {
        super(paymentRequest);
        this.paymentRequest = paymentRequest;
    }

    public PaymentRequest getPaymentRequest() {
        return this.paymentRequest;
    }
}
