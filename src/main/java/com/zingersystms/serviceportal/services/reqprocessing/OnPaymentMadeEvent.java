package com.zingersystms.serviceportal.services.reqprocessing;

import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import org.springframework.context.ApplicationEvent;

public class OnPaymentMadeEvent extends ApplicationEvent {

    private PaymentRequest paymentRequest;

    public OnPaymentMadeEvent(final PaymentRequest paymentRequest) {
        super(paymentRequest);
        this.paymentRequest = paymentRequest;
    }

    public PaymentRequest getPaymentRequest() {
        return this.paymentRequest;
    }
}
