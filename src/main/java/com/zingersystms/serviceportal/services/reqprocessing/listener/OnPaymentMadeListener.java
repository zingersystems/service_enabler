package com.zingersystms.serviceportal.services.reqprocessing.listener;

import com.zingersystms.serviceportal.persistence.model.NotificationType;
import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.NotificationSettingRepository;
import com.zingersystms.serviceportal.services.reqprocessing.OnPaymentMadeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class OnPaymentMadeListener implements ApplicationListener<OnPaymentMadeEvent> {
    private
    @Value("${service-portal.mail.support}")
    String support;

    private final JavaMailSender mailSender;

    @Resource
    private NotificationSettingRepository notificationSettingRepository;

    @Autowired
    public OnPaymentMadeListener(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnPaymentMadeEvent event) {
        if(notificationSettingRepository.findByNotificationType(NotificationType.ON_PAYMENT_REQUESTED).isActivated()){
            User user = event.getPaymentRequest().getRequest().getCreatedBy();
            if(user.getUserPreference().isEmailNotification()) {
                SimpleMailMessage userMailMessage = constructClientMailMessage(event.getPaymentRequest(), user);
                sendMail(userMailMessage);
            }
            User agent = event.getPaymentRequest().getRequest().getAgentId();
            if(agent.getUserPreference().isEmailNotification()) {
                SimpleMailMessage agentMailMessage = constructClientMailMessage(event.getPaymentRequest(), agent);
                sendMail(agentMailMessage);
            }
        }
    }

    @Async
    public void sendMail(SimpleMailMessage mailMessage) {
        this.mailSender.send(mailMessage);
    }

    private SimpleMailMessage constructClientMailMessage(PaymentRequest paymentRequest, User receiver) {
        String additionalMessage = "";
        String title;
        if(paymentRequest.isSettled()) {
            title = "Payment Successful for the service";
        } else {
            title = "Partial payment for the service";
            additionalMessage = paymentRequest.getExplicitComment();
        }
        String message = "Hello " + receiver + ","
                +"\n \n" + title + "\""
                + paymentRequest.getRequest().getServiceId().getName() + "\" "
                + "The details of the payment are: \n\n"
                + "Payment Request ID: " + paymentRequest.getUuid() + "\n"
                + "Amount: " + paymentRequest.getAmount() + "\n"
                + "Payment Request Motif: " + paymentRequest.getMotif() + "\n\n"
                + "Service ID: " + paymentRequest.getRequest().getServiceId().getUuid() + "\n\n"
                + "Service Request ID: " + paymentRequest.getRequest().getUuid() + "\n"
                + "Service Request Description: " + paymentRequest.getRequest().getDescription()+ "\n\n"
                + additionalMessage;

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(paymentRequest.getRequest().getCreatedBy().getEmail());
        email.setSubject("Payment Successful");
        email.setText(message);
        email.setFrom(support);
        return email;
    }
}
