package com.zingersystms.serviceportal.services.reqprocessing;

import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.persistence.model.Request;
import org.springframework.context.ApplicationEvent;

/**
 * @author kkyb on 8/22/17.
 */
public class OnCommentForRequestDoneEvent extends ApplicationEvent {

    private Comment comment;

    public OnCommentForRequestDoneEvent(final Comment comment) {
        super(comment);
        this.comment = comment;
    }

    public Comment getComment() {
        return this.comment;
    }
}
