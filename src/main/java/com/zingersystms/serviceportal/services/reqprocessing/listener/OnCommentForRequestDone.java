package com.zingersystms.serviceportal.services.reqprocessing.listener;

import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.persistence.model.NotificationType;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.NotificationSettingRepository;
import com.zingersystms.serviceportal.services.reqprocessing.OnCommentForRequestDoneEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class OnCommentForRequestDone implements ApplicationListener<OnCommentForRequestDoneEvent> {

    private
    @Value("${service-portal.mail.support}")
    String support;

    private final JavaMailSender mailSender;

    @Resource
    private NotificationSettingRepository notificationSettingRepository;

    @Autowired
    public OnCommentForRequestDone(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnCommentForRequestDoneEvent event) {
        Comment comment =  event.getComment();
        User receiver;
        if(!comment.getCreatedBy().equals(comment.getRequest().getCreatedBy())){
            receiver = comment.getRequest().getCreatedBy();
        }else{
            receiver = comment.getRequest().getAgentId();
        }
        if(receiver.getUserPreference().isEmailNotification() &&
                notificationSettingRepository.findByNotificationType(NotificationType.ON_COMMENT_FOR_REQUEST_DONE)
                        .isActivated()){
            SimpleMailMessage simpleMailMessage = constructMailMessage(comment, receiver);
            sendMail(simpleMailMessage);
        }
    }

    @Async
    public void sendMail(SimpleMailMessage simpleMailMessage) {
        mailSender.send(simpleMailMessage);
    }

    private SimpleMailMessage constructMailMessage(Comment comment, User receiver){

        String message = "Hello " + receiver.getFirstName() + ","
                +"\n\n" + comment.getCreatedBy() + " commented on the request for the service \""
                + comment.getRequest().getServiceId().getName() + "\"\n\n"
                + "Comment:\n"
                + "\"" + comment.getContent() + "\"\n\n"
                + "Thank you for using our services.";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(receiver.getEmail());
        email.setSubject("Comment made on Request");
        email.setText(message);
        email.setFrom(support);
        return email;
    }
}
