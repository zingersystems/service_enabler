package com.zingersystms.serviceportal.services.reqprocessing;

import com.zingersystms.serviceportal.persistence.model.Request;
import org.springframework.context.ApplicationEvent;

/**
 * @author kkyb on 8/23/17.
 */
public class OnRequestCreatedEvent extends ApplicationEvent {

    private Request request;
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public OnRequestCreatedEvent(Request source) {
        super(source);
        this.request = source;
    }

    public Request getRequest() {
        return request;
    }
}
