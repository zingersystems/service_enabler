package com.zingersystms.serviceportal.services.reqprocessing;


import com.zingersystms.serviceportal.persistence.model.Request;
import org.springframework.context.ApplicationEvent;

public class OnRequestStatusChangedEvent extends ApplicationEvent {

    private Request request;

    public OnRequestStatusChangedEvent(final Request request) {
        super(request);
        this.request = request;
    }

    public Request getRequest() {
        return this.request;
    }
}
