package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.web.dto.CommentOnlyDTO;
import org.springframework.stereotype.Component;

@Component
public interface CommentService {

    CommentOnlyDTO create(CommentOnlyDTO comment);

}
