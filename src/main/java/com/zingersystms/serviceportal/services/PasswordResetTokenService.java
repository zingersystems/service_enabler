package com.zingersystms.serviceportal.services;


import com.zingersystms.serviceportal.web.dto.GenericResponse;
import com.zingersystms.serviceportal.web.dto.PasswordResetDTO;

public interface PasswordResetTokenService {
    
    GenericResponse sendPasswordResetToken(String email);

    GenericResponse confirmPasswordReset(PasswordResetDTO passwordResetDTO);
}
