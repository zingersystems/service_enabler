package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.web.dto.PermissionUpdateDTO;
import com.zingersystms.serviceportal.web.dto.UserBareDTO;
import com.zingersystms.serviceportal.web.dto.UserDTO;
import com.zingersystms.serviceportal.web.dto.UserOnlyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface UserService {
    User register(User user);

    Page<UserDTO> findAll(Pageable pageable);

    User getUser(String uuid);

    UserDTO getUserDTO(String uuid);

    User findByUsernameCaseInsensitiveAndDeletedFalse(String username);

    UserDTO updateUserPermissions(PermissionUpdateDTO permissionUpdateDTO);

    User findByEmail(String email);

    void deleteUser(String uuid);

    UserOnlyDTO updateUser(UserBareDTO userBareDTO);

    UserDTO getCurrentUser();

    int getNumOfNonAdminUsers();
}
