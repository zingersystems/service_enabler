package com.zingersystms.serviceportal.services.passwordreset.listener;


import com.zingersystms.serviceportal.persistence.model.PasswordResetToken;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.PasswordResetTokenRepository;
import com.zingersystms.serviceportal.services.passwordreset.OnPasswordResetRequestEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class OnPasswordResetRequestListener implements ApplicationListener<OnPasswordResetRequestEvent> {

    @Autowired
    private JavaMailSender mailSender;
    private
    @Value("${service-portal.mail.verification.subject}")
    String subject;

    private
    @Value("${service-portal.mail.password-reset.url}")
    String passwordResetBaseUrl;

    private
    @Value("${service-portal.mail.verification.message}")
    String message;

    private
    @Value("${service-portal.mail.verification.denial}")
    String denial;

    private
    @Value("${service-portal.mail.support}")
    String support;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    @Override
    public void onApplicationEvent(OnPasswordResetRequestEvent event) {
        final String token = UUID.randomUUID()
                .toString();
        PasswordResetToken passwordResetToken = new PasswordResetToken(token, event.getUser());

        passwordResetTokenRepository.save(passwordResetToken);

        mailSender.send(constructEmailMessage(event.getUser(),token));
    }

    private final SimpleMailMessage constructEmailMessage(final User user, final String token) {
        final String recipientAddress = user.getEmail();
        final String greeting = "Hey " + user.getUsername();
        final String confirmationUrl = passwordResetBaseUrl + "/" + token;
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(greeting + " \r\n" + message + " \r\n" + confirmationUrl + " \r\n" + denial + " \r\n");
        email.setFrom(support);
        return email;
    }
}
