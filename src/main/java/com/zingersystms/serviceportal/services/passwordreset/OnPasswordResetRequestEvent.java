package com.zingersystms.serviceportal.services.passwordreset;


import com.zingersystms.serviceportal.persistence.model.User;
import org.springframework.context.ApplicationEvent;

public class OnPasswordResetRequestEvent extends ApplicationEvent {

    private final User user;

    public OnPasswordResetRequestEvent(final User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
