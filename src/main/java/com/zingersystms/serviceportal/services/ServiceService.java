package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.persistence.model.Service;
import com.zingersystms.serviceportal.web.dto.RequestOnlyDTO;
import com.zingersystms.serviceportal.web.dto.ServiceDTO;
import com.zingersystms.serviceportal.web.dto.ServiceOnlyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public interface ServiceService {

    Service findById(String uuid);

    ServiceDTO create(ServiceOnlyDTO service);

    Page<ServiceDTO> findAll(Pageable pageable);

    Page<ServiceOnlyDTO> findAllWithParams(Pageable pageable, String serviceCategoryUuid, long startDate, long endDate);

    ServiceDTO update(ServiceOnlyDTO service);

    ServiceDTO delete(String uuid);
}
