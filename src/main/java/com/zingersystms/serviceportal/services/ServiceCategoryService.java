package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import com.zingersystms.serviceportal.web.dto.ServiceCategoryDTO;
import com.zingersystms.serviceportal.web.dto.StatusDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ServiceCategoryService {

    ServiceCategory findByUuid(String uuid);

    ServiceCategoryDTO create(ServiceCategoryDTO serviceCategoryDTO);

    Page<ServiceCategoryDTO> findAll(Pageable pageable);

    ServiceCategoryDTO update(ServiceCategoryDTO serviceCategoryDTO);

    ServiceCategoryDTO delete(String uuid);
}
