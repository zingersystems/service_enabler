package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.web.dto.SimpleReportDataDTO;
import org.springframework.stereotype.Service;

public interface ReportService {
    SimpleReportDataDTO getSimpleReportData(int numOfMostRequestServices,
            int numOfMostPaidServices,
            int numOfMostProfitableServices,
            long startDate,
            long endDate);
}
