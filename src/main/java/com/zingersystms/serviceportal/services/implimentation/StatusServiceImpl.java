package com.zingersystms.serviceportal.services.implimentation;


import com.zingersystms.serviceportal.persistence.model.DefaultValue;
import com.zingersystms.serviceportal.persistence.model.KeyForDefaultValue;
import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.persistence.repository.DefaultValueRepository;
import com.zingersystms.serviceportal.persistence.repository.StatusRepository;
import com.zingersystms.serviceportal.services.StatusService;
import com.zingersystms.serviceportal.web.converters.StatusConverter;
import com.zingersystms.serviceportal.web.dto.StatusDTO;
import com.zingersystms.serviceportal.web.exceptions.StatusNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.StatusRequestsIsDefaultException;
import com.zingersystms.serviceportal.web.exceptions.StatusRequestsNotNullException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class StatusServiceImpl implements StatusService {

    @Resource
    private StatusRepository statusRepository;

    @Autowired
    private DefaultValueRepository defaultValueRepository;

    @Override
    @Transactional(rollbackFor = StatusNotFoundException.class)
    public Status findById(String uuid) {

        if (uuid == null)
            throw new StatusNotFoundException("There is no status with the id null");

        Status status = statusRepository.findByUuid(uuid);
        if (status == null) {
            throw new StatusNotFoundException("There is no status with the id " + uuid);
        }

        return status;
    }

    @Override
    @Transactional(rollbackFor = StatusNotFoundException.class)
    public Status findByName(String name) {

        Status status = statusRepository.findByName(name);
        if (status == null) {
            throw new StatusNotFoundException("There is no status with the name " + name);
        }

        return status;
    }

    @Override
    @Transactional
    public StatusDTO create(StatusDTO statusDTO) {

        Status status = new Status();
        status.setName(statusDTO.getName());
        status.setDescription(statusDTO.getDescription());

        return StatusConverter.convert(statusRepository.save(status));
    }

    @Override
    public Page<StatusDTO> findAll(Pageable pageable) {

        Page<Status> page = statusRepository.findAll(pageable);

        return new PageImpl<>(StatusConverter.convert(page.getContent()), pageable, page.getTotalElements());
    }

    @Override
    @Transactional(rollbackFor = StatusNotFoundException.class)
    public StatusDTO update(StatusDTO statusDTO) {
        Status status = statusRepository.findByUuid(statusDTO.getUuid());

        if (status == null) {
            throw new StatusNotFoundException("There is no status with the id " + statusDTO.getUuid());
        }

        status.setName(statusDTO.getName());
        status.setDescription(statusDTO.getDescription());
        status.setLastModifiedDate(new Date());

        return StatusConverter.convert(status);

    }

    @Override
    @Transactional(rollbackFor = {StatusNotFoundException.class, StatusRequestsNotNullException.class})
    public StatusDTO delete(String uuid) {

        Status status = statusRepository.findByUuid(uuid);

        if (status == null) {
            throw new StatusNotFoundException("There is no status with the id " + uuid);
        }

        if (status.getRequests().size() != 0)
            throw new StatusRequestsNotNullException("There may be more than one request for the status with id " + uuid + " you are trying to delete");

        if(status.getUuid().equals(defaultValueRepository
                .findByKeyForDefaultValue(KeyForDefaultValue.DEFAULT_REQUEST_STATUS).getUuid())) {
            throw new StatusRequestsIsDefaultException("This status is the default status for new created requests, please make another status the default before deleting this one");
        }

        statusRepository.delete(status);

        return StatusConverter.convert(status);
    }

    @Override
    public StatusDTO setDefaultStatus(String uuid) {
        Status status = statusRepository.findByUuid(uuid);
        if (status == null) {
            throw new StatusNotFoundException("There is no status with the uuid " + uuid);
        }

        DefaultValue defaultValue = defaultValueRepository
                .findByKeyForDefaultValue(KeyForDefaultValue.DEFAULT_REQUEST_STATUS);
        defaultValue.setDefaultValueReference(uuid);
        defaultValueRepository.save(defaultValue);
        return new StatusDTO(status);
    }
}
