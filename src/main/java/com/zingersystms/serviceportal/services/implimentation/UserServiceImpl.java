package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.Authorities;
import com.zingersystms.serviceportal.persistence.model.Authority;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.AuthorityRepository;
import com.zingersystms.serviceportal.persistence.repository.UserRepository;
import com.zingersystms.serviceportal.services.UserService;
import com.zingersystms.serviceportal.services.registration.OnRegistrationCompleteEvent;
import com.zingersystms.serviceportal.web.converters.UserConverter;
import com.zingersystms.serviceportal.web.dto.PermissionUpdateDTO;
import com.zingersystms.serviceportal.web.dto.UserBareDTO;
import com.zingersystms.serviceportal.web.dto.UserDTO;
import com.zingersystms.serviceportal.web.dto.UserOnlyDTO;
import com.zingersystms.serviceportal.web.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    StandardPasswordEncoder standardPasswordEncoder;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Resource
    private UserRepository userRepository;

    @Resource
    private AuthorityRepository authorityRepository;

    @Override
    @Transactional(rollbackFor = {EmailAlreadyExistException.class,
            UsernameAlreadyExistException.class,
            PhoneNumberAlreadyExistException.class})
    public User register(User user) {
        User emailUser = userRepository.findByEmailAndDeletedFalse(user.getEmail());

        if (emailUser != null)
            throw new EmailAlreadyExistException("The email " + user.getEmail() + " you are trying to use is already being use by another user ");

        User usernameUser = userRepository.findByUsernameIgnoreCaseAndDeletedFalse(user.getUsername());

        if (usernameUser != null)
            throw new UsernameAlreadyExistException("The username " + user.getUsername() + " you are trying to use is already being use by another user ");

        User phoneNumberUser = userRepository.findByPhoneNumberAndDeletedFalse(user.getPhoneNumber());

        if(phoneNumberUser != null)
            throw new PhoneNumberAlreadyExistException("The phone number" + user.getPhoneNumber() + " you are trying to use is already being used by another user");

        user.setPassword(standardPasswordEncoder.encode(user.getPassword()));
        User registeredUser = userRepository.save(user);

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registeredUser));

        return registeredUser;
    }

    @Override
    public Page<UserDTO> findAll(Pageable pageable) {
        Page<User> page = userRepository.findByDeletedFalse(pageable);

        return new PageImpl<>(UserConverter.convert(page.getContent()), pageable, page.getTotalElements());
    }

    @Override
    public UserDTO getUserDTO(String uuid) {
        User user = userRepository.findByUuidAndDeletedFalse(uuid);
        if (user == null)
            throw new UserNotFoundException("The user with the id  " + uuid + " does not exist");

        return new UserDTO(user);
    }

    @Override
    public User findByUsernameCaseInsensitiveAndDeletedFalse(String username) {
        User user = userRepository.findByUsernameIgnoreCaseAndDeletedFalse(username);
        if (user == null)
            throw new UserNotFoundException("The user with the username  " + username + " does not exist");

        return user;
    }

    @Override
    @Transactional(rollbackFor = {UserNotFoundException.class, UserAlreadyHasPermissionException.class})
    public UserDTO updateUserPermissions(PermissionUpdateDTO permissionUpdateDTO) {
        User user = userRepository.findByUuidAndDeletedFalse(permissionUpdateDTO.getUser_id());
        if (user == null)
            throw new UserNotFoundException("The user with the id  " + permissionUpdateDTO.getUser_id() + " does not exist");

        if (user.getAuthorities()
                .contains(authorityRepository
                        .findOne(permissionUpdateDTO.getAuthority().toString())))
            throw new UserAlreadyHasPermissionException("User already has the role being requested");

        user.getAuthorities().add(authorityRepository.findOne(permissionUpdateDTO.getAuthority().toString()));


        return UserConverter.convert(user);
    }

    @Override
    public User findByEmail(String email) {
        User user = userRepository.findByEmailAndDeletedFalse(email);
        if (user == null)
            throw new UserNotFoundException("The user with the email  " + email + " does not exist");
        return user;
    }

    @Override
    public void deleteUser(String uuid) {
        User user = userRepository.findByUuidAndDeletedFalse(uuid);
        if(user == null) {
            throw new UserNotFoundException("The user with uuid " + uuid + " does not exist");
        }
        if (!user.getAuthorities().contains(new Authority("ROLE_ADMIN"))) {
            user.setDeleted(true);
            userRepository.save(user);
        }else {
            throw new NotEnoughPermissionException("You do not have enough exception to carry out this action");
        }
    }

    @Override
    @Transactional
    public UserOnlyDTO updateUser(UserBareDTO userBareDTO) {

        org.springframework.security.core.userdetails.User user = User.getSpringCurrentUser();

        User savedUser = userRepository.findByUsernameIgnoreCaseAndDeletedFalse(user.getUsername());
        if(savedUser == null) {
            throw new UserNotFoundException("This user have been deleted");
        }
        savedUser.setFirstName(userBareDTO.getFirstName());
        savedUser.setLastName(userBareDTO.getLastName());
        savedUser.setCountry(userBareDTO.getCountry());
        savedUser.setPhoneNumber(userBareDTO.getPhoneNumber());
        savedUser.setPhotoUrl(userBareDTO.getPhotoUrl());
        savedUser.getUserPreference().setEmailNotification(userBareDTO.isEmailNotification());

        return new UserOnlyDTO(savedUser);
    }

    @Override
    public UserDTO getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        return new UserDTO(userRepository.findByUsernameIgnoreCaseAndDeletedFalse(user.getUsername()));
    }

    @Override
    public int getNumOfNonAdminUsers() {
        return this.userRepository.countByAuthoritiesIsNotAndActivatedTrueAndDeletedFalse(this.authorityRepository
                .findOne(Authorities.ROLE_ADMIN.name()));
    }

    @Override
    public User getUser(String uuid) {
        if (uuid == null)
            throw new UserNotFoundException("Please send a valid uuid to request for a user");

        User user = userRepository.findByUuidAndDeletedFalse(uuid);
        if (user == null)
            throw new UserNotFoundException("The user with the id  " + uuid + " does not exist");

        return user;
    }
}
