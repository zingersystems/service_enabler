package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.StoredFile;
import com.zingersystms.serviceportal.persistence.repository.StoredFileRepository;
import com.zingersystms.serviceportal.services.StorageService;
import com.zingersystms.serviceportal.web.dto.StoredFileDTO;
import com.zingersystms.serviceportal.web.exceptions.StorageException;
import com.zingersystms.serviceportal.web.exceptions.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private StoredFileRepository storedFileRepository;

    @Autowired
    public FileSystemStorageService(StoredFileRepository storedFileRepository,
                                    @Value("${service-portal.storage.file-system.root-location}") final String rootPath) {
        this.rootLocation = Paths.get(rootPath);
        this.storedFileRepository = storedFileRepository;
    }

    @Override
    @Transactional(rollbackFor = {StorageException.class})
    public StoredFileDTO store(MultipartFile file) {

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        StoredFile storedFile = new StoredFile();
        storedFile.setFilename(filename);
        storedFile.setSize(file.getSize());
        storedFile.setFileExtension(StringUtils.getFilenameExtension(file.getOriginalFilename()));
        storedFile = storedFileRepository.save(storedFile);
        this.storeFile(file, storedFile.getUuid());
        return new StoredFileDTO(storedFile);
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        }
        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            if(!Files.exists(rootLocation)){
                Files.createDirectories(rootLocation);
            }
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }


    private void storeFile(MultipartFile file, String filename) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                    StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }
}
