package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.Authorities;
import com.zingersystms.serviceportal.persistence.repository.*;
import com.zingersystms.serviceportal.services.ReportService;
import com.zingersystms.serviceportal.web.dto.SimpleReportDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ReportServiceImpl implements ReportService {

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    private final ServiceCategoryRepository serviceCategoryRepository;

    private final ServiceRepository serviceRepository;

    private final PaymentRequestRepository paymentRequestRepository;

    private final RequestRepository requestRepository;

    @Autowired
    public ReportServiceImpl(UserRepository userRepository, AuthorityRepository authorityRepository, ServiceCategoryRepository serviceCategoryRepository, ServiceRepository serviceRepository, PaymentRequestRepository paymentRequestRepository, RequestRepository requestRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.serviceCategoryRepository = serviceCategoryRepository;
        this.serviceRepository = serviceRepository;
        this.paymentRequestRepository = paymentRequestRepository;
        this.requestRepository = requestRepository;
    }

    @Override
    public SimpleReportDataDTO getSimpleReportData(
            int numOfMostRequestedServices,
            int numOfMostPaidServices,
            int numOfMostProfitableServices,
            long startDate,
            long endDate) {

        Date startDateObject = new Date(0);
        if(startDate != 0) {
            startDateObject.setTime(startDate);
        }
        Date endDateObject = new Date();
        if(endDate != 0) {
            endDateObject.setTime(endDate);
        }

        SimpleReportDataDTO simpleReportDataDTO = new SimpleReportDataDTO();
        simpleReportDataDTO.setNumOfUsers(this.userRepository
                .countByAuthoritiesIsNotAndActivatedTrueAndDeletedFalseAndCreatedDateBetween(
                this.authorityRepository.findOne(Authorities.ROLE_ADMIN.name()), startDateObject, endDateObject));
        simpleReportDataDTO.setNumOfServiceCategories(this.serviceCategoryRepository
                .countAllByCreatedDateBetween(startDateObject, endDateObject));
        simpleReportDataDTO.setNumOfServices(this.serviceRepository
                .countAllByCreatedDateBetween(startDateObject, endDateObject));
        simpleReportDataDTO.setNumOfServiceRequests(this.requestRepository
                .countAllByCreatedDateBetween(startDateObject, endDateObject));
        simpleReportDataDTO.setNumOfPaymentRequests(this.paymentRequestRepository
                .countAllByCreatedDateBetween(startDateObject, endDateObject));
        simpleReportDataDTO.setNumOfPaymentRequestsSettled(this.paymentRequestRepository
                .countAllBySettledTrueAndLastModifiedDateIsBetween(startDateObject, endDateObject));
        Double totalIncome = paymentRequestRepository.findTotalAmount(startDateObject, endDateObject);
        simpleReportDataDTO.setTotalIncome(totalIncome == null ? 0 : totalIncome);
        simpleReportDataDTO.setMostRequestedServices(this.serviceRepository.getMostRequestedServices(startDateObject, endDateObject));
        simpleReportDataDTO.setMostPaidForServices(this.serviceRepository.getMostPaidForServices(startDateObject, endDateObject));
//        simpleReportDataDTO.setMostProfitableServices(this.serviceRepository.getMostProfitableServices());
        return simpleReportDataDTO;

    }
}
