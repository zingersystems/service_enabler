package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import com.zingersystms.serviceportal.persistence.repository.ServiceCategoryRepository;
import com.zingersystms.serviceportal.services.ServiceCategoryService;
import com.zingersystms.serviceportal.web.converters.ServiceCategoryConverter;
import com.zingersystms.serviceportal.web.dto.ServiceCategoryDTO;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.ServiceCategoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServiceCategoryImpl implements ServiceCategoryService {

    private final ServiceCategoryRepository serviceCategoryRepository;

    @Autowired
    public ServiceCategoryImpl(ServiceCategoryRepository serviceCategoryRepository) {
        this.serviceCategoryRepository = serviceCategoryRepository;
    }

    @Override
    @Transactional(rollbackFor = ServiceCategoryNotFoundException.class)
    public ServiceCategory findByUuid(String uuid) {
        ServiceCategory serviceCategory = this.serviceCategoryRepository.findByUuid(uuid);
        if(serviceCategory == null) {
            throw new ServiceCategoryNotFoundException("There is no service category with the uuid " + uuid);
        }
        return serviceCategory;
    }

    @Override
    public ServiceCategoryDTO create(ServiceCategoryDTO serviceCategoryDTO) {
        ServiceCategory serviceCategory = new ServiceCategory();
        serviceCategory.setName(serviceCategoryDTO.getName());
        serviceCategory.setDescription(serviceCategoryDTO.getDescription());
        serviceCategory = this.serviceCategoryRepository.save(serviceCategory);
        return new ServiceCategoryDTO(serviceCategory);
    }

    @Override
    public Page<ServiceCategoryDTO> findAll(Pageable pageable) {

        Page<ServiceCategory> page = this.serviceCategoryRepository.findAll(pageable);

        return new PageImpl<>(ServiceCategoryConverter.convert(page.getContent()), pageable, page.getTotalElements());
    }

    @Override
    @Transactional(rollbackFor = ServiceCategoryNotFoundException.class)
    public ServiceCategoryDTO update(ServiceCategoryDTO serviceCategoryDTO) {

        ServiceCategory serviceCategory = this.serviceCategoryRepository.findByUuid(serviceCategoryDTO.getUuid());
        if(serviceCategory == null) {
            throw new ServiceCategoryNotFoundException("There is no service category with the uuid " +
                    serviceCategoryDTO.getUuid());
        }
        serviceCategory.setName(serviceCategoryDTO.getName());
        serviceCategory.setDescription(serviceCategoryDTO.getDescription());
        serviceCategory = this.serviceCategoryRepository.save(serviceCategory);
        return new ServiceCategoryDTO(serviceCategory);
    }

    @Override
    @Transactional(rollbackFor = ServiceCategoryNotFoundException.class)
    public ServiceCategoryDTO delete(String uuid) {

        ServiceCategory serviceCategory = this.serviceCategoryRepository.findByUuid(uuid);
        if(serviceCategory == null) {
            throw new ServiceCategoryNotFoundException("There is no service category with the uuid " + uuid);
        }
        this.serviceCategoryRepository.delete(serviceCategory);
        return new ServiceCategoryDTO(serviceCategory);
    }
}
