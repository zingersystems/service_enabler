package com.zingersystms.serviceportal.services.implimentation;


import com.zingersystms.serviceportal.persistence.model.PasswordResetToken;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.PasswordResetTokenRepository;
import com.zingersystms.serviceportal.services.PasswordResetTokenService;
import com.zingersystms.serviceportal.services.UserService;
import com.zingersystms.serviceportal.services.passwordreset.OnPasswordResetRequestEvent;
import com.zingersystms.serviceportal.web.dto.GenericResponse;
import com.zingersystms.serviceportal.web.dto.PasswordResetDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

@Repository
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {

    @Autowired
    StandardPasswordEncoder standardPasswordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    @Override
    public GenericResponse sendPasswordResetToken(String email) {
        final User user = userService.findByEmail(email);

        eventPublisher.publishEvent(new OnPasswordResetRequestEvent(user));

        return new GenericResponse("Password Reset Mail sent successfully");
    }

    @Override
    @Transactional(rollbackFor = {InvalidTokenException.class})
    public GenericResponse confirmPasswordReset(PasswordResetDTO passwordResetDTO) {

        PasswordResetToken passwordResetToken = passwordResetTokenRepository.findByToken(passwordResetDTO.getToken());
        if (passwordResetToken == null) {
            throw new InvalidTokenException("Sorry there was a problem with the token . Please request for a new verification email");
        }

        final User user = passwordResetToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((passwordResetToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            passwordResetTokenRepository.delete(passwordResetToken);
            throw new InvalidTokenException("Sorry there was a problem with the token . Please request for a new verification email");
        }

        user.setPassword(standardPasswordEncoder.encode(passwordResetDTO.getPassword()));

        passwordResetTokenRepository.delete(passwordResetToken);

        return new GenericResponse("200","Password reset successful");
    }
}
