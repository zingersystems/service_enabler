package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.PaymentMethod;
import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import com.zingersystms.serviceportal.persistence.repository.PaymentMethodRepository;
import com.zingersystms.serviceportal.services.PaymentMethodService;
import com.zingersystms.serviceportal.web.dto.PaymentMethodDTO;
import com.zingersystms.serviceportal.web.dto.PaymentRequestDTO;
import com.zingersystms.serviceportal.web.exceptions.PaymentMethodNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PaymentMethodServiceImpl implements PaymentMethodService{

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Override
    public PaymentMethodDTO findByUuid(String uuid) {
        PaymentMethod paymentMethod = paymentMethodRepository.findByUuid(uuid);
        if(paymentMethod == null) {
            throw new PaymentMethodNotFoundException("The payment method with the uuid " + uuid + " does not exists");
        }
        return new PaymentMethodDTO(paymentMethod);
    }

    @Override
    public PaymentMethodDTO create(PaymentMethodDTO paymentMethodDTO) {
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setPaymentMethodLogo(paymentMethodDTO.getPaymentMethodLogo());
        paymentMethod.setPaymentUrl(paymentMethodDTO.getPaymentUrl());
        paymentMethod = paymentMethodRepository.save(paymentMethod);
        return new PaymentMethodDTO(paymentMethod);
    }

    @Override
    @Transactional(rollbackFor = {PaymentMethodNotFoundException.class})
    public PaymentMethodDTO update(PaymentMethodDTO paymentMethodDTO) {
        PaymentMethod paymentMethod = paymentMethodRepository.findByUuid(paymentMethodDTO.getUuid());
        if(paymentMethod == null) {
            throw new PaymentMethodNotFoundException("The payment method with the uuid "
                    + paymentMethodDTO.getUuid() + " does not exists");
        }
        paymentMethod.setPaymentMethodLogo(paymentMethodDTO.getPaymentMethodLogo());
        paymentMethod.setPaymentUrl(paymentMethodDTO.getPaymentUrl());
        paymentMethod = paymentMethodRepository.save(paymentMethod);
        return new PaymentMethodDTO(paymentMethod);
    }

    @Override
    @Transactional(rollbackFor = {PaymentMethodNotFoundException.class})
    public PaymentMethodDTO delete(String uuid) {
        PaymentMethod paymentMethod = paymentMethodRepository.findByUuid(uuid);
        if(paymentMethod == null) {
            throw new PaymentMethodNotFoundException("The payment method with the uuid "
                    + uuid + " does not exists");
        }
        paymentMethodRepository.delete(paymentMethod);
        return new PaymentMethodDTO(paymentMethod);
    }

    @Override
    public Page<PaymentMethodDTO> findAll(Pageable pageable) {
        Page<PaymentMethod> paymentMethodPage = this.paymentMethodRepository.findAll(pageable);
        List<PaymentMethodDTO> paymentMethodDTOS = paymentMethodPage.getContent()
                .stream()
                .map(PaymentMethodDTO::new)
                .collect(Collectors.toList());

        return new PageImpl<>(paymentMethodDTOS, pageable, paymentMethodPage.getTotalElements());
    }
}
