package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.Service;
import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.persistence.repository.ServiceCategoryRepository;
import com.zingersystms.serviceportal.persistence.repository.ServiceRepository;
import com.zingersystms.serviceportal.services.ServiceCategoryService;
import com.zingersystms.serviceportal.services.ServiceService;
import com.zingersystms.serviceportal.web.converters.RequestConverter;
import com.zingersystms.serviceportal.web.converters.ServiceConverter;
import com.zingersystms.serviceportal.web.dto.RequestOnlyDTO;
import com.zingersystms.serviceportal.web.dto.ServiceDTO;
import com.zingersystms.serviceportal.web.dto.ServiceOnlyDTO;
import com.zingersystms.serviceportal.web.exceptions.ServiceCategoryNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.ServiceNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.ServiceRequestsNotNullException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    @Resource
    private ServiceRepository serviceRepository;

    @Resource
    private ServiceCategoryRepository serviceCategoryRepository;

    @Autowired
    private ServiceCategoryService serviceCategoryService;

    @Override
    @Transactional(rollbackFor = ServiceNotFoundException.class)
    public Service findById(String uuid) {

        Service service = serviceRepository.findByUuid(uuid);

        if (service == null)
            throw new ServiceNotFoundException("There is no service with the id " + uuid);

        return service;
    }

    @Override
    @Transactional
    public ServiceDTO create(ServiceOnlyDTO serviceOnlyDTO) {

        ServiceCategory serviceCategory = this.serviceCategoryRepository
                .findByUuid(serviceOnlyDTO.getServiceCategoryDTOUuid());

        if(serviceCategory == null){
            throw new ServiceCategoryNotFoundException("No service category correspond to the uuid "
                    + serviceOnlyDTO.getServiceCategoryDTOUuid());
        }

        Service service = new Service();

        service.setName(serviceOnlyDTO.getName());
        service.setDescription(serviceOnlyDTO.getDescription());
        service.setPhoto(serviceOnlyDTO.getPhoto());
        service.setServiceCategory(serviceCategory);

        return ServiceConverter.convert(serviceRepository.save(service));
    }

    @Override
    public Page<ServiceDTO> findAll(Pageable pageable) {
        Page<Service> page = serviceRepository.findAll(pageable);

        return new PageImpl<>(ServiceConverter.convert(page.getContent()), pageable, page.getTotalElements());
    }

    @Override
    public Page<ServiceOnlyDTO> findAllWithParams(Pageable pageable, String serviceCategoryUuid, long startDate, long endDate) {
        Date startDateObject = new Date(0);
        if(startDate != 0) {
            startDateObject.setTime(startDate);
        }
        Date endDateObject = new Date();
        if(endDate != 0) {
            endDateObject.setTime(endDate);
        }
        Page<Service> services;
        if(serviceCategoryUuid == null || serviceCategoryUuid.isEmpty()) {
            services = this.serviceRepository.findAllByCreatedDateBetween(pageable, startDateObject, endDateObject);
        }else {
            ServiceCategory serviceCategory = this.serviceCategoryService.findByUuid(serviceCategoryUuid);
            services = this.serviceRepository.findAllByServiceCategoryAndCreatedDateBetween(pageable, serviceCategory,
                    startDateObject, endDateObject);
        }

        List<ServiceOnlyDTO> serviceOnlyDTOS = ServiceConverter.convertOnly(services.getContent());
        return new PageImpl<>(serviceOnlyDTOS, pageable, services.getTotalElements());
    }

    @Override
    @Transactional(rollbackFor = ServiceNotFoundException.class)
    public ServiceDTO update(ServiceOnlyDTO service) {
        Service oldService = serviceRepository.findByUuid(service.getUuid());

        if (oldService == null)
            throw new ServiceNotFoundException("There is no service with the id " + service.getUuid());

        oldService.setName(service.getName());
        oldService.setDescription(service.getDescription());
        oldService.setPhoto(service.getPhoto());
        oldService.setLastModifiedDate(new Date());

        return ServiceConverter.convert(oldService);
    }

    @Override
    @Transactional(rollbackFor = {ServiceNotFoundException.class, ServiceRequestsNotNullException.class})
    public ServiceDTO delete(String uuid) {

        Service service = serviceRepository.findByUuid(uuid);

        if (service == null)
            throw new ServiceNotFoundException("There is no service with the id " + uuid);

        if (service.getRequests().size() != 0)
            throw new ServiceRequestsNotNullException("There may be more than one request for the service with id " + uuid + " you are trying to delete");

        serviceRepository.delete(service);

        return ServiceConverter.convert(service);
    }
}
