package com.zingersystms.serviceportal.services.implimentation;


import com.zingersystms.serviceportal.persistence.model.Authority;
import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.persistence.repository.CommentRepository;
import com.zingersystms.serviceportal.persistence.repository.RequestRepository;
import com.zingersystms.serviceportal.services.RequestService;
import com.zingersystms.serviceportal.services.ServiceService;
import com.zingersystms.serviceportal.services.StatusService;
import com.zingersystms.serviceportal.services.reqprocessing.OnRequestCreatedEvent;
import com.zingersystms.serviceportal.services.reqprocessing.OnRequestStatusChangedEvent;
import com.zingersystms.serviceportal.web.converters.CommentConverter;
import com.zingersystms.serviceportal.web.converters.RequestConverter;
import com.zingersystms.serviceportal.web.dto.CommentInRequestDTO;
import com.zingersystms.serviceportal.web.dto.RequestBareDTO;
import com.zingersystms.serviceportal.web.dto.RequestDTO;
import com.zingersystms.serviceportal.web.dto.RequestOnlyDTO;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    private final Logger log = LoggerFactory.getLogger(RequestServiceImpl.class);

    @Resource
    private RequestRepository requestRepository;

    @Resource
    private UserServiceImpl userService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    @Transactional(rollbackFor = RequestNotFoundException.class)
    public Request findById(String uuid) {

        Request request = requestRepository.findByUuid(uuid);
        if (request == null) {
            log.debug("request with uuid " + uuid + "not found");
            throw new RequestNotFoundException("There is no request with the id " + uuid);
        }
        return request;
    }

    @Override
    @Transactional
    public RequestDTO create(RequestBareDTO request) {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();
        Request newRequest = new Request();
        com.zingersystms.serviceportal.persistence.model.User user = userService
                .findByUsernameCaseInsensitiveAndDeletedFalse(
                        ((User) authentication.getPrincipal()).getUsername());
        newRequest.setCreatedBy(user);
        newRequest.setLastModifiedBy(user);
        newRequest.setDescription(request.getDescription());
        newRequest.setDocument(request.getDocument());
        newRequest.setServiceId(serviceService.findById(request.getService()));

        newRequest.setStatusId(statusService.findByName("Pending processing"));

        newRequest.setAgentId(userService.findByUsernameCaseInsensitiveAndDeletedFalse("agent"));

        newRequest = requestRepository.save(newRequest);

        eventPublisher.publishEvent(new OnRequestCreatedEvent(newRequest));

        return RequestConverter.convert(newRequest);

    }

    @Override
    public Page<RequestDTO> findAll(Pageable pageable) {

        Page<Request> page = requestRepository.findAll(pageable);

        return new PageImpl<>(RequestConverter.convert(page.getContent()), pageable, page.getTotalElements());


    }

    @Override
    public Page<RequestOnlyDTO> findAllWithParams(Pageable pageable, String statusUuid, long startDate, long endDate) {
        Date startDateObject = new Date(0);
        if(startDate != 0) {
            startDateObject.setTime(startDate);
        }
        Date endDateObject = new Date();
        if(endDate != 0) {
            endDateObject.setTime(endDate);
        }
        Page<Request> requests;
        if(statusUuid == null || statusUuid.isEmpty()) {
            requests = this.requestRepository.findAllByCreatedDateBetween(pageable, startDateObject, endDateObject);
        }else {
            Status status = this.statusService.findById(statusUuid);
            requests = this.requestRepository.findAllByStatusIdAndCreatedDateBetween(pageable, status,
                    startDateObject, endDateObject);
        }

        List<RequestOnlyDTO> requestOnlyDTOS = RequestConverter.convertOnlyList(requests.getContent());
        if(requestOnlyDTOS == null) {
            requestOnlyDTOS = new ArrayList<>(0);
        }
        return new PageImpl<>(requestOnlyDTOS, pageable, requests.getTotalElements());
    }

    @Override
    public Page<RequestOnlyDTO> findAllByStatusUuid(Pageable pageable, String statusUuid) {
        Page<Request> requests = this.requestRepository.findAllByStatusId(
                this.statusService.findById(statusUuid),
                pageable);
        List<RequestOnlyDTO> requestOnlyDTOS = RequestConverter.convertOnlyList(requests.getContent());
        if(requestOnlyDTOS == null) {
            requestOnlyDTOS = new ArrayList<>(0);
        }
        return new PageImpl<>(requestOnlyDTOS, pageable, requests.getTotalElements());
    }

    @Override
    @Transactional(rollbackFor = RequestNotFoundException.class)
    public RequestDTO update(RequestBareDTO request) {

        boolean isRequestStatusChanged = false;

        Request oldRequest = requestRepository.findByUuid(request.getUuid());

        if (oldRequest == null)
            throw new RequestNotFoundException("There is no request with the id " + request.getUuid());

        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        com.zingersystms.serviceportal.persistence.model.User user = userService
                .findByUsernameCaseInsensitiveAndDeletedFalse(
                        ((User) authentication.getPrincipal()).getUsername());
        oldRequest.setLastModifiedBy(user);

        oldRequest.setDescription(request.getDescription());
        oldRequest.setServiceId(serviceService.findById(request.getService()));

        if (request.getAgent() != null && user.getAuthorities().contains(new Authority("ROLE_ADMIN"))) {
            oldRequest.setAgentId(userService.getUser(request.getAgent()));
        }

        if (user.getAuthorities().contains(new Authority("ROLE_ADMIN"))) {
            Status status = statusService.findById(request.getStatus());
            if(status != null && !oldRequest.getStatusId().equals(status)){
                isRequestStatusChanged = true;
            }
            oldRequest.setStatusId(status);
        }
        oldRequest.setDocument(request.getDocument());

        oldRequest = requestRepository.save(oldRequest);

        if(isRequestStatusChanged && oldRequest != null){
            eventPublisher.publishEvent(new OnRequestStatusChangedEvent(oldRequest));
        }

        return RequestConverter.convert(oldRequest);
    }

    @Override
    @Transactional(rollbackFor = RequestNotFoundException.class)
    public RequestDTO delete(String uuid) {

        Request request = requestRepository.findByUuid(uuid);

        if (request == null)
            throw new RequestNotFoundException("There is no request with the id " + uuid);

        requestRepository.delete(request);

        return RequestConverter.convert(request);
    }

    @Override
    public Page<CommentInRequestDTO> getCommentsForRequest(String id, Pageable pageable) {

        Request request = requestRepository.findByUuid(id);

        if (request == null)
            throw new RequestNotFoundException("There is no request with the id " + id);

        Page<Comment> comments = commentRepository.findByRequest(request, pageable);

        return new PageImpl<>(CommentConverter.convert(comments.getContent()), pageable, comments.getTotalElements());


    }
}
