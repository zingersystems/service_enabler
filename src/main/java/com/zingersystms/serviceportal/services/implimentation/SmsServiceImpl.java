package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.services.SmsService;
import com.zingersystms.serviceportal.web.exceptions.SmsNotSentException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class SmsServiceImpl implements SmsService{

    private static final int SUCCESS_STATUS_RESPOND = 1701;

    @Value("${service-portal.sms.username}")
    private String username;

    @Value("${service-portal.sms.password}")
    private String password;

    @Value("${service-portal.sms.type}")
    private String type;

    @Value("${service-portal.sms.dlr}")
    private String dlr;

    @Value("${service-portal.sms.source}")
    private String source;

    @Value("${service-portal.sms.url}")
    private String url;

    @Value("${service-portal.sms.port}")
    private String port;


    @Override
    public void sendMessage(String phoneNumber, String message) {
        RestTemplate restTemplate = new RestTemplate();
        String endpoint = "" +
                url +
                ":" +
                port +
                "/bulksms/bulksms";

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(endpoint)
                .queryParam("username", username)
                .queryParam("password", password)
                .queryParam("type", type)
                .queryParam("dlr", dlr)
                .queryParam("source", source)
                .queryParam("destination", phoneNumber)
                .queryParam("message", message);


        ResponseEntity<String> response = restTemplate.postForEntity(builder.build().toUri(), null, String.class);
        if(response.getStatusCodeValue() != SUCCESS_STATUS_RESPOND){
            throw new SmsNotSentException(response.toString());
        }
    }
}
