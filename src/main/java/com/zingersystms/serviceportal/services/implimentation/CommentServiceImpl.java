package com.zingersystms.serviceportal.services.implimentation;


import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.persistence.repository.CommentRepository;
import com.zingersystms.serviceportal.services.CommentService;
import com.zingersystms.serviceportal.services.RequestService;
import com.zingersystms.serviceportal.services.reqprocessing.OnCommentForRequestDoneEvent;
import com.zingersystms.serviceportal.web.converters.CommentConverter;
import com.zingersystms.serviceportal.web.dto.CommentOnlyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private UserServiceImpl userService;

    @Resource
    private RequestService requestService;

    @Resource
    private CommentRepository commentRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public CommentOnlyDTO create(CommentOnlyDTO comment) {
        Comment newComment = new Comment();
        com.zingersystms.serviceportal.persistence.model.User user = userService
                .findByUsernameCaseInsensitiveAndDeletedFalse(
                        com.zingersystms.serviceportal.persistence.model.User
                        .getSpringCurrentUser()
                        .getUsername());
        newComment.setCreatedBy(user);
        newComment.setContent(comment.getContent());
        newComment.setRequest(requestService.findById(comment.getRequest()));
        newComment.setAttachmentURL(comment.getAttachmentUrl());
        newComment = commentRepository.save(newComment);
        eventPublisher.publishEvent(new OnCommentForRequestDoneEvent(newComment));
        return CommentConverter.convert(newComment);
    }

}
