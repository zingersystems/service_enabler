package com.zingersystms.serviceportal.services.implimentation;


import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.model.VerificationToken;
import com.zingersystms.serviceportal.persistence.repository.AuthorityRepository;
import com.zingersystms.serviceportal.persistence.repository.VerificationTokenRepository;
import com.zingersystms.serviceportal.services.UserService;
import com.zingersystms.serviceportal.services.VerificationTokenService;
import com.zingersystms.serviceportal.services.registration.OnRegistrationCompleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Calendar;

@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    public void createVerificationTokenForUser(User user, String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        verificationTokenRepository.save(myToken);
    }

    @Override
    public VerificationToken getVerificationToken(String VerificationToken) {
        return null;
    }

    @Override
    public void resendVerificationToken(String email) {

        final User user = userService.findByEmail(email);

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user));
    }

    @Override
    public void validateVerificationToken(String token) {
        final VerificationToken verificationToken = verificationTokenRepository.findByToken(token);
        if (verificationToken == null) {
            throw new InvalidTokenException("Sorry there was a problem with the token . Please request for a new verification email");
        }

        final User user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            verificationTokenRepository.delete(verificationToken);
//            return TOKEN_EXPIRED;
        }

        user.setActivated(true);
        user.setAuthorities(Arrays.asList(authorityRepository.findOne("ROLE_USER")));

        verificationTokenRepository.delete(verificationToken);

//        return TOKEN_VALID;
    }


}
