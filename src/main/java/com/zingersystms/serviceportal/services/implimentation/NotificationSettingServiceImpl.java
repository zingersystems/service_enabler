package com.zingersystms.serviceportal.services.implimentation;

import com.zingersystms.serviceportal.persistence.model.NotificationSetting;
import com.zingersystms.serviceportal.persistence.model.NotificationType;
import com.zingersystms.serviceportal.persistence.repository.NotificationSettingRepository;
import com.zingersystms.serviceportal.services.NotificationSettingService;
import com.zingersystms.serviceportal.web.dto.NotificationSettingDTO;
import com.zingersystms.serviceportal.web.exceptions.NotificationSettingTypeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationSettingServiceImpl implements NotificationSettingService {

    private final NotificationSettingRepository notificationSettingRepository;

    @Autowired
    public NotificationSettingServiceImpl(NotificationSettingRepository notificationSettingRepository) {
        this.notificationSettingRepository = notificationSettingRepository;
    }

    @Override
    public NotificationSettingDTO updateNotificationSetting(NotificationSettingDTO notificationSettingDTO) {

        NotificationSetting notificationSetting =
                this.notificationSettingRepository.findByNotificationType(notificationSettingDTO.getNotificationType());
        notificationSetting.setActivated(notificationSettingDTO.isActivated());
        this.notificationSettingRepository.save(notificationSetting);
        notificationSettingDTO.setUuid(notificationSetting.getUuid());
        return notificationSettingDTO;
    }

    @Override
    public NotificationSettingDTO getNotificationSetting(String type) {
        NotificationType notificationType;
        try {
            notificationType = NotificationType.valueOf(type);
        }catch (IllegalArgumentException ex){
            throw new NotificationSettingTypeNotFoundException("The notification setting of type " + type + " does not exists");
        }

        NotificationSetting notificationSetting = this.notificationSettingRepository
                .findByNotificationType(notificationType);
        NotificationSettingDTO notificationSettingDTO = new NotificationSettingDTO();
        notificationSettingDTO.setUuid(notificationSetting.getUuid());
        notificationSettingDTO.setNotificationType(notificationType);
        notificationSettingDTO.setActivated(notificationSetting.isActivated());
        return notificationSettingDTO;
    }
}
