package com.zingersystms.serviceportal.services.implimentation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.repository.PaymentRequestRepository;
import com.zingersystms.serviceportal.services.PaymentRequestService;
import com.zingersystms.serviceportal.services.RequestService;
import com.zingersystms.serviceportal.services.reqprocessing.OnPaymentMadeEvent;
import com.zingersystms.serviceportal.services.reqprocessing.OnPaymentRequestMadeEvent;
import com.zingersystms.serviceportal.util.MigVpcPaymentUrlGenerator;
import com.zingersystms.serviceportal.web.dto.*;
import com.zingersystms.serviceportal.web.exceptions.NotEnoughPermissionException;
import com.zingersystms.serviceportal.web.exceptions.PaymentRequestNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

//import java.io.IOException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PaymentRequestServiceImpl implements PaymentRequestService {

    private final PaymentRequestRepository paymentRequestRepository;

    private final RequestService requestService;

    private final ApplicationEventPublisher eventPublisher;

    @Value("${service-portal.payment.momo.request-url}")
    String momoPaymentRequestUrl;

    @Value("${service-portal.payment.momo.merchant-email}")
    String momoPaymentRequestEmail;

    private final MigVpcPaymentUrlGenerator paymentUrlGenerator;

    @Autowired
    public PaymentRequestServiceImpl(PaymentRequestRepository paymentRequestRepository, RequestService requestService,
                                     ApplicationEventPublisher eventPublisher, MigVpcPaymentUrlGenerator paymentUrlGenerator) {

        this.paymentRequestRepository = paymentRequestRepository;
        this.requestService = requestService;
        this.eventPublisher = eventPublisher;
        this.paymentUrlGenerator = paymentUrlGenerator;
    }

    @Override
    @Transactional(rollbackFor = RequestNotFoundException.class)
    public PaymentRequestDTO findByUuid(String uuid) {
        PaymentRequest paymentRequest = this.paymentRequestRepository.findByUuid(uuid);
        if(paymentRequest == null){
            throw new PaymentRequestNotFoundException("The payment request with UUID " + uuid + " was not found");
        }
        return new PaymentRequestDTO(paymentRequest);
    }

    @Override
    @Transactional(rollbackFor = RequestNotFoundException.class)
    public PaymentRequestDTO create(PaymentRequestBareDTO paymentRequestBareDTO) {

        Request request = requestService.findById(paymentRequestBareDTO.getRequestUuid());
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setRequest(request);
        paymentRequest.setAmount(paymentRequestBareDTO.getAmount());
        paymentRequest.setMotif(paymentRequestBareDTO.getMotif());
        paymentRequest.setSettled(paymentRequestBareDTO.isSettled());
        paymentRequest = this.paymentRequestRepository.save(paymentRequest);
        this.eventPublisher.publishEvent(new OnPaymentRequestMadeEvent(paymentRequest));
        return new PaymentRequestDTO(paymentRequest);
    }

    @Override
    @Transactional(rollbackFor = PaymentRequestNotFoundException.class)
    public PaymentRequestDTO update(PaymentRequestBareDTO paymentRequestBareDTO) {
        PaymentRequest paymentRequest = this.paymentRequestRepository.findByUuid(paymentRequestBareDTO.getUuid());
        if(paymentRequest == null){
            throw new PaymentRequestNotFoundException("The payment request with UUID " +
                    paymentRequestBareDTO.getUuid() +
                    " was not found");
        }
        paymentRequest.setSettled(paymentRequestBareDTO.isSettled());
        paymentRequest.setMotif(paymentRequestBareDTO.getMotif());
        paymentRequest.setAmount(paymentRequestBareDTO.getAmount());
        paymentRequest = this.paymentRequestRepository.save(paymentRequest);
        return new PaymentRequestDTO(paymentRequest);
    }

    @Override
    @Transactional(rollbackFor = PaymentRequestNotFoundException.class)
    public PaymentRequestDTO delete(String uuid) {
        PaymentRequest paymentRequest = this.paymentRequestRepository.findByUuid(uuid);
        if(paymentRequest == null){
            throw new PaymentRequestNotFoundException("The payment request with UUID " +
                    uuid +
                    " was not found");
        }
        this.paymentRequestRepository.delete(paymentRequest);
        return new PaymentRequestDTO(paymentRequest);
    }

    @Override
    public Page<PaymentRequestDTO> findAll(Pageable pageable) {

        Page<PaymentRequest> paymentRequestPage = this.paymentRequestRepository.findAll(pageable);
        List<PaymentRequestDTO> paymentRequestDTOs = paymentRequestPage.getContent()
                .stream()
                .map(PaymentRequestDTO::new)
                .collect(Collectors.toList());

        return new PageImpl<>(paymentRequestDTOs, pageable, paymentRequestPage.getTotalElements());
    }

    @Override
    public Page<PaymentRequestDTO> findForCurrentUser(Pageable pageable) {
        Page<PaymentRequest> paymentRequestPage = paymentRequestRepository
                .findByRequestCreatedByUsername(User.getSpringCurrentUser().getUsername(), pageable);
        List<PaymentRequestDTO> paymentRequestDTOs = paymentRequestPage.getContent()
                .stream()
                .map(PaymentRequestDTO::new)
                .collect(Collectors.toList());

        return new PageImpl<>(paymentRequestDTOs, null, paymentRequestPage.getTotalElements());
    }

    @Override
    public GenericResponse processMoMoPayment(MoMoPaymentInfo moMoPaymentInfo) {

        PaymentRequest paymentRequest = paymentRequestRepository.findByUuid(moMoPaymentInfo.getPaymentRequestUuid());
        if (paymentRequest == null) {
            throw new PaymentRequestNotFoundException("The payment request with UUID " +
                    moMoPaymentInfo.getPaymentRequestUuid() +
                    " was not found");
        }

        if (!paymentRequest.getRequest().getCreatedBy().getUsername()
                .equals(User.getSpringCurrentUser().getUsername())) {
            throw new NotEnoughPermissionException ("Warning you are trying to pay for the request of" +
                    paymentRequest.getRequest().getCreatedBy());
        }

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        UriComponentsBuilder builder = composeMoMoUrl(paymentRequest.getAmount(), moMoPaymentInfo.getUserPhoneNumber());
        HttpEntity entity = new HttpEntity(headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response =
                restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, String.class);
        System.out.println("*****************Result*************");
        System.out.println(response.getBody());
        System.out.println("*****************End of Result*************");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode responseBody;
        try {
            responseBody = mapper.readTree(response.getBody());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        paymentRequest.setShortComment(responseBody.get("TransactionID").asText());
        paymentRequest.setExplicitComment("{\"ProcessingNumber\":"
                + responseBody.get("ProcessingNumber").asText()
                + ", \"StatusDesc\":\"" + responseBody.get("StatusDesc").asText()
                + "\", \"Amount\":\"" + responseBody.get("Amount").asText()
                + "\", \"SenderNumber\":\"" + responseBody.get("SenderNumber").asText() + "\"}");
        GenericResponse genericResponse;
        if(responseBody.get("StatusCode").asText().equals("01")) {
            paymentRequest.setSettled(true);
            paymentRequestRepository.save(paymentRequest);
            genericResponse = new GenericResponse("Transaction successful");
            this.eventPublisher.publishEvent(new OnPaymentMadeEvent(paymentRequest));
        }else {
            genericResponse  =  new GenericResponse("Transaction not successful");
            genericResponse.setError("Transaction not successful");
        }

        return genericResponse;
    }

    @Override
    public GenericResponse processMockPayment(MoMoPaymentInfo moMoPaymentInfo) {
        PaymentRequest paymentRequest = paymentRequestRepository.findByUuid(moMoPaymentInfo.getPaymentRequestUuid());
        if (paymentRequest == null) {
            throw new PaymentRequestNotFoundException("The payment request with UUID " +
                    moMoPaymentInfo.getPaymentRequestUuid() +
                    " was not found");
        }

        if (!paymentRequest.getRequest().getCreatedBy().getUsername()
                .equals(User.getSpringCurrentUser().getUsername())) {
            throw new NotEnoughPermissionException ("Warning you are trying to pay for the request of" +
                    paymentRequest.getRequest().getCreatedBy());
        }

        GenericResponse genericResponse;

        if(moMoPaymentInfo.getUserPhoneNumber().equals("670258299")
                || moMoPaymentInfo.getUserPhoneNumber().equals("674592970")
                || moMoPaymentInfo.getUserPhoneNumber().equals("675264729")) {
            paymentRequest.setSettled(true);
            genericResponse = new GenericResponse("Transaction successful");
            paymentRequest.setShortComment("TransactionID:142921733");
            paymentRequest.setExplicitComment("{\"OperationDate\":\"2017-09-08 17:23:14\",\"Amount\":\"1\",\"StatusDesc\":\"Successfully processed transaction.\",\"SenderNumber\":\"237675264729\"}");
        }else {
            genericResponse  =  new GenericResponse("Transaction not successful");
            genericResponse.setError("Transaction not successful");
        }
        this.paymentRequestRepository.save(paymentRequest);
        return genericResponse;
    }

    @Override
    public GenericResponse getVpcUrl(VpcPaymentInfo vpcPaymentInfo) {
        PaymentRequest paymentRequest = paymentRequestRepository.findByUuid(vpcPaymentInfo.getPaymentRequestUuid());
        if (paymentRequest == null) {
            throw new PaymentRequestNotFoundException("The payment request with UUID " +
                    vpcPaymentInfo.getPaymentRequestUuid() +
                    " was not found");
        }

        if (!paymentRequest.getRequest().getCreatedBy().getUsername()
                .equals(User.getSpringCurrentUser().getUsername())) {
            throw new NotEnoughPermissionException ("Warning you are trying to pay for the request of" +
                    paymentRequest.getRequest().getCreatedBy());
        }
        GenericResponse genericResponse;
        try {
           genericResponse = new GenericResponse(this.paymentUrlGenerator.getVpcURL(paymentRequest,
                   vpcPaymentInfo.getRefreshToken()));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            genericResponse = new GenericResponse("");
            genericResponse.setError("Internal Error while generating the url");
        }
        paymentRequestRepository.save(paymentRequest);
        return genericResponse;
    }

    @Override
    public GenericResponse processVpcResponse(Map<String, String> allParams) {
        MigVpcPaymentUrlGenerator.ReducedTransactionResponse reducedTransactionResponse;

        GenericResponse genericResponse;
        try {
            reducedTransactionResponse = this.paymentUrlGenerator.reduceTransactionResponse(allParams);
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            genericResponse = new GenericResponse("Internal Error while generating the url");
            genericResponse.setError("not_display");
            return genericResponse;
        }
        if(!reducedTransactionResponse.isResponseValid()) {
            genericResponse = new GenericResponse("Bad request");
            genericResponse.setError("not_display");
            return genericResponse;
        }
        String paymentRequestUuid = reducedTransactionResponse.getMerchTxnRef();
        paymentRequestUuid = paymentRequestUuid.substring(0, paymentRequestUuid.indexOf('/'));
        PaymentRequest paymentRequest = paymentRequestRepository.findByUuid(paymentRequestUuid);
        if (paymentRequest == null) {
            genericResponse = new GenericResponse("The payment request with UUID " +
                    paymentRequestUuid +
                    " was not found");
            genericResponse.setError("not_display");
            return genericResponse;
        }
        if(!reducedTransactionResponse.isSuccessful()) {
            genericResponse = new GenericResponse(reducedTransactionResponse.getMessage());
            genericResponse.setError("Transaction not successful");
            paymentRequest.setShortComment("VpcOrderInfo: " + reducedTransactionResponse.getOrderInfo());
            paymentRequest.setExplicitComment("Transaction not successful");
            paymentRequest.setNumberOfPaymentTries(paymentRequest.getNumberOfPaymentTries() + 1);
            this.paymentRequestRepository.save(paymentRequest);
            return genericResponse;
        }

        if(Integer.toString(new Double(paymentRequest.getAmount()).intValue())
                .equals(reducedTransactionResponse.getAmount())) {
            paymentRequest.setSettled(true);
            paymentRequest.setShortComment(reducedTransactionResponse.getReceiptNo());
            paymentRequest.setExplicitComment(reducedTransactionResponse.getMessage() + "\n"
                    + reducedTransactionResponse.getTxnResponseCodeDescription());
            this.paymentRequestRepository.save(paymentRequest);
            genericResponse = new GenericResponse("Transaction successful");
            this.eventPublisher.publishEvent(new OnPaymentMadeEvent(paymentRequest));
            return genericResponse;
        }else {
            paymentRequest.setShortComment("VpcReceiptNo: " + reducedTransactionResponse.getReceiptNo());
            paymentRequest.setExplicitComment(reducedTransactionResponse.getMessage() + "\n"
                    + reducedTransactionResponse.getTxnResponseCodeDescription() + "\n"
                    + "Amount: " + reducedTransactionResponse.getAmount());
            paymentRequest.setNumberOfPaymentTries(paymentRequest.getNumberOfPaymentTries() + 1);
            this.paymentRequestRepository.save(paymentRequest);
            genericResponse = new GenericResponse("Amount not completely paid\nAmount paid: " + reducedTransactionResponse.getAmount());
            genericResponse.setError("Amount not completely paid");
            this.eventPublisher.publishEvent(new OnPaymentMadeEvent(paymentRequest));
            return genericResponse;
        }
    }

    private UriComponentsBuilder composeMoMoUrl(Double amount, String phoneNumber) {

        return UriComponentsBuilder.fromHttpUrl(momoPaymentRequestUrl)
                .queryParam("_amount", amount.intValue())
                .queryParam("_email", momoPaymentRequestEmail)
                .queryParam("_tel", phoneNumber);
    }
}
