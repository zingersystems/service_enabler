package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.web.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * @author kkyb on 8/16/17.
 */
public interface PaymentRequestService {

    PaymentRequestDTO findByUuid(String uuid);

    PaymentRequestDTO create(PaymentRequestBareDTO paymentRequestBareDTO);

    PaymentRequestDTO update(PaymentRequestBareDTO paymentRequestBareDTO);

    PaymentRequestDTO delete(String uuid);

    Page<PaymentRequestDTO> findAll(Pageable pageable);

    Page<PaymentRequestDTO> findForCurrentUser(Pageable pageable);

    GenericResponse processMoMoPayment(MoMoPaymentInfo moMoPaymentInfo);

    GenericResponse processMockPayment(MoMoPaymentInfo moMoPaymentInfo);

    GenericResponse getVpcUrl(VpcPaymentInfo vpcPaymentInfo);

    GenericResponse processVpcResponse(Map<String, String> allParams);
}
