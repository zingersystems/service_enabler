package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.persistence.model.VerificationToken;
import org.springframework.stereotype.Component;

@Component
public interface VerificationTokenService {

    void createVerificationTokenForUser(User user, String token);

    VerificationToken getVerificationToken(String VerificationToken);

    void resendVerificationToken(String email);

    void validateVerificationToken(String token);

}
