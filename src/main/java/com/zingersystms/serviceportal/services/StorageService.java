package com.zingersystms.serviceportal.services;

import com.zingersystms.serviceportal.persistence.model.StoredFile;
import com.zingersystms.serviceportal.web.dto.StoredFileDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    StoredFileDTO store(MultipartFile file);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();
}
