package com.zingersystms.serviceportal.web.exceptions;

public class PaymentRequestNotFoundException extends RuntimeException {
    public PaymentRequestNotFoundException(String message) {
        super(message);
    }
}
