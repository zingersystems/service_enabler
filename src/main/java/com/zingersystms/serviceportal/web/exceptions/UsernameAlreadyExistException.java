package com.zingersystms.serviceportal.web.exceptions;


public class UsernameAlreadyExistException extends RuntimeException {
    public UsernameAlreadyExistException(String message) {
        super(message);
    }
}
