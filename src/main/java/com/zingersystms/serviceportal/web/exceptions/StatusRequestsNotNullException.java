package com.zingersystms.serviceportal.web.exceptions;


public class StatusRequestsNotNullException extends RuntimeException {

    public StatusRequestsNotNullException(String message) {
        super(message);
    }

}
