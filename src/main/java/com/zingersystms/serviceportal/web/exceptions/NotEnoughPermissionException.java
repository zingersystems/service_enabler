package com.zingersystms.serviceportal.web.exceptions;

public class NotEnoughPermissionException extends RuntimeException{
    public NotEnoughPermissionException(String message) {
        super(message);
    }
}
