package com.zingersystms.serviceportal.web.exceptions;

public class ServiceCategoryNotFoundException extends RuntimeException {
    public ServiceCategoryNotFoundException(String message) {
        super(message);
    }
}
