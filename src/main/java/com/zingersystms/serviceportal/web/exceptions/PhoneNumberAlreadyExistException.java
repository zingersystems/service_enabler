package com.zingersystms.serviceportal.web.exceptions;

public class PhoneNumberAlreadyExistException extends RuntimeException {
    public PhoneNumberAlreadyExistException(String message) {
        super(message);
    }
}
