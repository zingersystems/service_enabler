package com.zingersystms.serviceportal.web.exceptions;

public class NotificationSettingTypeNotFoundException extends RuntimeException {
    public NotificationSettingTypeNotFoundException(String message){
        super(message);
    }
}
