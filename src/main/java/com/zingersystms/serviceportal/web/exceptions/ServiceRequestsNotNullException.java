package com.zingersystms.serviceportal.web.exceptions;


public class ServiceRequestsNotNullException extends RuntimeException {
    public ServiceRequestsNotNullException() {
    }

    public ServiceRequestsNotNullException(String message) {
        super(message);
    }

    public ServiceRequestsNotNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceRequestsNotNullException(Throwable cause) {
        super(cause);
    }

    public ServiceRequestsNotNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
