package com.zingersystms.serviceportal.web.exceptions;

public class SmsNotSentException extends RuntimeException {

    public SmsNotSentException(String message) {
        super(message);
    }
}
