package com.zingersystms.serviceportal.web.exceptions;

public class StatusRequestsIsDefaultException extends RuntimeException {
    public StatusRequestsIsDefaultException(String message) {
        super(message);
    }
}
