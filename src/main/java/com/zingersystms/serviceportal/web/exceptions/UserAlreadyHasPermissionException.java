package com.zingersystms.serviceportal.web.exceptions;


public class UserAlreadyHasPermissionException extends RuntimeException {
    public UserAlreadyHasPermissionException(String message) {
        super(message);
    }
}
