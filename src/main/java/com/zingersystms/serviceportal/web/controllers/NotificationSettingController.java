package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.services.NotificationSettingService;
import com.zingersystms.serviceportal.web.dto.NotificationSettingDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/notificationSettings")
@Api(value = "notificationSettings")
@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
public class NotificationSettingController {

    private final NotificationSettingService notificationSettingService;

    @Autowired
    public NotificationSettingController(NotificationSettingService notificationSettingService) {
        this.notificationSettingService = notificationSettingService;
    }

    @PutMapping(value = "update", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @ApiOperation(value = "update a notification setting", response = NotificationSettingDTO.class)
    NotificationSettingDTO updateNotificationSetting(@RequestBody @Valid NotificationSettingDTO notificationSettingDTO) {
        return this.notificationSettingService.updateNotificationSetting(notificationSettingDTO);
    }

    @GetMapping(value = "/{type}", produces = "application/json")
    @ResponseBody
    @ApiOperation(value = "returns the notification setting which correspond to the given type", response = NotificationSettingDTO.class)
    NotificationSettingDTO getNotificationSetting(@PathVariable String type) {
        return this.notificationSettingService.getNotificationSetting(type);
    }
}
