package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.services.ServiceCategoryService;
import com.zingersystms.serviceportal.web.dto.GenericResponse;
import com.zingersystms.serviceportal.web.dto.ServiceCategoryDTO;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.ServiceCategoryNotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/serviceCategory")
public class ServiceCategoryController {

    private final ServiceCategoryService serviceCategoryService;

    @Autowired
    public ServiceCategoryController(ServiceCategoryService serviceCategoryService) {
        this.serviceCategoryService = serviceCategoryService;
    }

    @GetMapping(value = "/{uuid}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    @ApiOperation(value = "Returns the service category corresponding to this uuid",response = ServiceCategoryDTO.class)
    ServiceCategoryDTO getServiceCategory(@PathVariable String uuid) {

        return new ServiceCategoryDTO(serviceCategoryService.findByUuid(uuid));

    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiOperation(value = "Returns the service category created with a uuid",response = ServiceCategoryDTO.class)
    ServiceCategoryDTO createServiceCategory(@RequestBody @Valid ServiceCategoryDTO serviceCategoryDTO) {

        return serviceCategoryService.create(serviceCategoryDTO);

    }

    @GetMapping(value = "", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    @ApiOperation(value = "Returns the a page of service categories",response = ServiceCategoryDTO.class)
    Page<ServiceCategoryDTO> getServiceCategoryPage(Pageable pageable) {

        return serviceCategoryService.findAll(pageable);

    }

    @PutMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiOperation(value = "Returns the updated service category",response = ServiceCategoryDTO.class)
    ServiceCategoryDTO editServiceCategory(@RequestBody @Valid ServiceCategoryDTO serviceCategoryDTO) {

        return serviceCategoryService.update(serviceCategoryDTO);

    }

    @DeleteMapping(value = "/{uuid}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiOperation(value = "Returns the deleted service category",response = ServiceCategoryDTO.class)
    ServiceCategoryDTO deleteServiceCategory(@PathVariable String uuid) {

        return serviceCategoryService.delete(uuid);

    }

    @ExceptionHandler(ServiceCategoryNotFoundException.class)
    ResponseEntity<GenericResponse> serviceCategoryNotFoundException(RequestNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
