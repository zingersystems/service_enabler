package com.zingersystms.serviceportal.web.controllers;


import com.zingersystms.serviceportal.services.ServiceService;
import com.zingersystms.serviceportal.web.converters.ServiceConverter;
import com.zingersystms.serviceportal.web.dto.GenericResponse;
import com.zingersystms.serviceportal.web.dto.ServiceDTO;
import com.zingersystms.serviceportal.web.dto.ServiceOnlyDTO;
import com.zingersystms.serviceportal.web.exceptions.ServiceCategoryNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.ServiceNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.ServiceRequestsNotNullException;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/services")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @GetMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    @ApiOperation(value = "Returns json data about a service",response = ServiceDTO.class)
    ServiceDTO showService(@PathVariable String id) {

        return ServiceConverter.convert(serviceService.findById(id));

    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiOperation(value = "Create a new Service",response = ServiceDTO.class)
    ServiceDTO createNewService(@RequestBody @Valid ServiceOnlyDTO service) {

        return serviceService.create(service);

    }

    @GetMapping(value = "", produces = "application/json")
    @ResponseBody
    @ApiOperation(value="Get all services with parameters",
            authorizations = {@Authorization(value="oauth2",scopes = {@AuthorizationScope(scope="entity.read",description = "Read access on entity in my new API")})})
    @ApiResponses({@ApiResponse(code = 401, message = "Not Authenticated", response = GenericResponse.class)})
    Page<ServiceOnlyDTO> serviceListPage(Pageable pageable,
                                     @RequestParam(value = "service_category_uuid", defaultValue = "") String serviceCategoryUuid,
                                     @RequestParam(value = "start_date", defaultValue = "0") long startDate,
                                     @RequestParam(value = "end_date", defaultValue = "0") long endDate) {

        return serviceService.findAllWithParams(pageable, serviceCategoryUuid, startDate, endDate);

    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    ServiceDTO editService(@RequestBody @Valid ServiceOnlyDTO service,
                           @PathVariable String id) {

        return serviceService.update(service);

    }


    @DeleteMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    ServiceDTO deleteService(@PathVariable String id) {

        return serviceService.delete(id);

    }

    @ExceptionHandler(ServiceNotFoundException.class)
    ResponseEntity<GenericResponse> serviceNotFoundException(ServiceNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ServiceRequestsNotNullException.class)
    ResponseEntity<GenericResponse> serviceRequestsNotNullException(ServiceRequestsNotNullException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ServiceCategoryNotFoundException.class)
    ResponseEntity<GenericResponse> serviceCategoryNotFoundException(ServiceCategoryNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
