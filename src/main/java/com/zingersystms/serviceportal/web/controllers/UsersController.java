package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.services.PasswordResetTokenService;
import com.zingersystms.serviceportal.services.UserService;
import com.zingersystms.serviceportal.services.VerificationTokenService;
import com.zingersystms.serviceportal.web.dto.*;
import com.zingersystms.serviceportal.web.exceptions.EmailAlreadyExistException;
import com.zingersystms.serviceportal.web.exceptions.NotEnoughPermissionException;
import com.zingersystms.serviceportal.web.exceptions.UserNotActivatedException;
import com.zingersystms.serviceportal.web.exceptions.UsernameAlreadyExistException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping(value = "/users")
public class UsersController {

	@Autowired
	VerificationTokenService verificationTokenService;

	@Autowired
	PasswordResetTokenService passwordResetTokenService;

	@Autowired
	private UserService userService;

    @Value("${service-portal.account-confirmation-redirect-url}")
    private String accountConfirmationRedirectUrl;

	@PostMapping(path = "/register")
	public UserDTO register(@RequestBody @Valid User user) {
		return new UserDTO(userService.register(user));
	}

	@GetMapping(path = "")
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public Page<UserDTO> getAllUsers(Pageable pageable) {
		return userService.findAll(pageable);
	}

	@GetMapping(path = "/{id}")
	UserDTO getUser(@PathVariable("id") final String uuid) {
		return userService.getUserDTO(uuid);
	}

	@GetMapping(path = "/me")
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
	UserDTO getCurrentUser() {
		return userService.getCurrentUser();
	}

	@PostMapping(path = "/permissions")
	@ApiOperation(value = "Updates a users permissions", notes = "Requires a users Id and the permission")
	UserDTO updateUserPermissions(@RequestBody @Valid PermissionUpdateDTO permissionUpdateDTO) {
		return userService.updateUserPermissions(permissionUpdateDTO);
	}

	@GetMapping(path = "/verify-registration-token")
	void handleRegistrationVerification(HttpServletResponse response, @RequestParam("token") final String token)
			throws IOException {
		verificationTokenService.validateVerificationToken(token);
		response.sendRedirect("http://www.google.com");
	}

	@PostMapping(path = "/resend-registration-token")
	void handleResendRegistrationToken(@RequestParam("email") final String email) {
		verificationTokenService.resendVerificationToken(email);
	}

	@PostMapping(path = "/reset-password")
	@ResponseBody
	public GenericResponse resetPassword(final HttpServletRequest request, @RequestParam("email") final String email) {
		return passwordResetTokenService.sendPasswordResetToken(email);
	}

	@PostMapping(path = "/confirm-password-reset")
	@ResponseBody
	public GenericResponse confirmPasswordReset(@RequestBody @Valid PasswordResetDTO passwordResetDTO) {
		return passwordResetTokenService.confirmPasswordReset(passwordResetDTO);
	}

	@PostMapping(path = "/delete/{uuid}")
	@ResponseBody
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public GenericResponse deleteUser(@PathVariable("uuid") String uuid) {
		this.userService.deleteUser(uuid);
		return new GenericResponse("User deleted successfully");
	}

    @PutMapping(path = "/update")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    public UserOnlyDTO updateUserBasicInfo(@RequestBody @Valid UserBareDTO userBareDTO) {
        return this.userService.updateUser(userBareDTO);
    }


    @GetMapping(path = "/report/get-num-of-users")
    @ResponseBody
//    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    public int getNumOfUsers() {
        return this.userService.getNumOfNonAdminUsers();
    }


	@ExceptionHandler(UserNotActivatedException.class)
	ResponseEntity<GenericResponse> userNotActivatedException(UserNotActivatedException e) {
		GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)),
				e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(EmailAlreadyExistException.class)
	ResponseEntity<GenericResponse> emailAlreadyExistException(EmailAlreadyExistException e) {
		GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.BAD_REQUEST)),
				e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UsernameAlreadyExistException.class)
	ResponseEntity<GenericResponse> usernameAlreadyExistException(UsernameAlreadyExistException e) {
		GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.BAD_REQUEST)),
				e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

    @ExceptionHandler(NotEnoughPermissionException.class)
    ResponseEntity<GenericResponse> notEnoughPermissionException(NotEnoughPermissionException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.BAD_REQUEST)),
                e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
