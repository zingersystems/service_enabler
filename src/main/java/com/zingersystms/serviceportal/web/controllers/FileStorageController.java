package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.persistence.model.StoredFile;
import com.zingersystms.serviceportal.persistence.repository.StoredFileRepository;
import com.zingersystms.serviceportal.services.StorageService;
import com.zingersystms.serviceportal.web.dto.StoredFileDTO;
import com.zingersystms.serviceportal.web.exceptions.StorageFileNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
@RequestMapping(value = "/files")
@Api(value = "files", description = "Operations pertaining to file downloading")
public class FileStorageController {

    private final StorageService storageService;

    private final StoredFileRepository storedFileRepository;

    @Autowired
    public FileStorageController(StorageService storageService, StoredFileRepository storedFileRepository) {
        this.storageService = storageService;
        this.storedFileRepository = storedFileRepository;
    }

    @GetMapping("/{filename}")
    @ApiOperation(value = "Download the file corresponding to the uuid", response = ResponseEntity.class)
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        System.out.println(filename);

        Resource file = storageService.loadAsResource(filename);
        StoredFile storedFile = storedFileRepository.findByUuid(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "." + storedFile.getFileExtension() + "\"").body(file);
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    @ApiOperation(value = "Upload a file on the server", response = StoredFile.class)
    public StoredFileDTO handleFileUpload(@RequestParam("file") MultipartFile file) {

        return storageService.store(file);
    }

    @GetMapping("meta/{filename}")
    @ApiOperation(value = "Download the file corresponding to the uuid", response = ResponseEntity.class)
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    public ResponseEntity getFileMeta(@PathVariable String filename) {
        StoredFile storedFile = this.storedFileRepository.findByUuid(filename);
        if(storedFile == null){
            return ResponseEntity.notFound().build();
        }else {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8")
                    .body(storedFile);
        }
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
