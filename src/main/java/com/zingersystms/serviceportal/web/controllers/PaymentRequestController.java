package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.services.PaymentRequestService;
import com.zingersystms.serviceportal.web.dto.*;
import com.zingersystms.serviceportal.web.exceptions.PaymentRequestNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/paymentRequests")
@Api(value = "payment request", description = "Operations pertaining to payment requests in Service Portal")
public class PaymentRequestController {

    private final PaymentRequestService paymentRequestService;

    @Value("${service-portal.account-confirmation-redirect-url}")
    private String redirectionBasicUrl;

    @Autowired
    public PaymentRequestController(PaymentRequestService paymentRequestService) {
        this.paymentRequestService = paymentRequestService;
    }

    @ApiOperation(value = "Find a peyment request by uuid", response = PaymentRequestDTO.class)
    @GetMapping(value = "/{uuid}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    PaymentRequestDTO getPaymentRequest(@PathVariable String uuid) {

        return paymentRequestService.findByUuid(uuid);

    }

    @ApiOperation(value = "Find payment request for the current user", response = PaymentRequestDTO.class)
    @GetMapping(value = "/me", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    Page<PaymentRequestDTO> getMyPaymentRequest(Pageable pageable) {

        return paymentRequestService.findForCurrentUser(pageable);

    }

    @ApiOperation(value = "Create a new payment request", response = PaymentRequestDTO.class)
    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    PaymentRequestDTO createNewPaymentRequest(@RequestBody @Valid PaymentRequestBareDTO paymentRequest) {

        return paymentRequestService.create(paymentRequest);

    }

    @ApiOperation(value = "find all payment requests", response = PaymentRequestDTO.class)
    @GetMapping(value = "", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Not Authenticated", response = GenericResponse.class)

    })
    Page<PaymentRequestDTO> paymentRequestListPage(Pageable pageable) {

        return paymentRequestService.findAll(pageable);

    }

    @ApiOperation(value = "update payment requests", response = PaymentRequestDTO.class)
    @PutMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    PaymentRequestDTO updatePaymentRequest(@RequestBody @Valid PaymentRequestBareDTO request) {

        return paymentRequestService.update(request);

    }

    @ApiOperation(value = "delete a payment requests", response = PaymentRequestDTO.class)
    @DeleteMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    PaymentRequestDTO deleteRequest(@PathVariable String id) {

        return paymentRequestService.delete(id);

    }

    @ApiOperation(value = "make momo payment", response = PaymentRequestDTO.class)
    @PostMapping(value = "/process/momo", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    GenericResponse processMoMoPayment(@RequestBody @Valid MoMoPaymentInfo moMoPaymentInfo) {

        return paymentRequestService.processMoMoPayment(moMoPaymentInfo);
    }

    @ApiOperation(value = "make a mock payment", response = GenericResponse.class)
    @PostMapping(value = "/process/mock", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    GenericResponse processMockPayment(@RequestBody @Valid MoMoPaymentInfo moMoPaymentInfo) {

        return paymentRequestService.processMockPayment(moMoPaymentInfo);
    }

    @ApiOperation(value = "make a vpc payment", response = GenericResponse.class)
    @PostMapping(value = "/process/vpc", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    GenericResponse getVpcUrl(@RequestBody @Valid VpcPaymentInfo vpcPaymentInfo) {

        return paymentRequestService.getVpcUrl(vpcPaymentInfo);
    }

    @ApiOperation(value = "make a vpc payment", response = GenericResponse.class)
    @GetMapping(value = "/process/vpc", produces = "application/json")
    @ResponseBody
    void handleVpcPaymentProcessingResponse(HttpServletResponse response, @RequestParam Map<String, String> allParams)
            throws IOException {
        String transactionUuid = allParams.get("vpc_MerchTxnRef");
        String redirectionUrl = this.redirectionBasicUrl
                + "#" + "/" + "single-payment-request" + "/"
                + transactionUuid.substring(0, transactionUuid.indexOf('/')) + "/"
                + allParams.get("user_RefreshToken") + "/vpc";
        GenericResponse genericResponse = paymentRequestService.processVpcResponse(allParams);
        if((genericResponse.getError() != null && !genericResponse.getError().equals("not_display"))
                || genericResponse.getMessage().equals("Transaction successful")) {
            response.sendRedirect(redirectionUrl);
        }
    }


    @ExceptionHandler(RequestNotFoundException.class)
    ResponseEntity<GenericResponse> requestNotFoundException(RequestNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PaymentRequestNotFoundException.class)
    ResponseEntity<GenericResponse> paymentRequestNotFoundException(RequestNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
