package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.services.CommentService;
import com.zingersystms.serviceportal.web.dto.CommentOnlyDTO;
import com.zingersystms.serviceportal.web.dto.GenericResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/comments")
@PreAuthorize("hasAnyAuthority('ROLE_USER')")
@Api(value = "comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping(path = "", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create a new comment for a service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comment successfully created", response = CommentOnlyDTO.class),
            @ApiResponse(code = 404, message = "Request Not found", response = GenericResponse.class),
            @ApiResponse(code = 401, message = "Not Authenticated", response = GenericResponse.class)
    })
    CommentOnlyDTO creatComment(@RequestBody @Valid CommentOnlyDTO comment) {

        return commentService.create(comment);

    }
}
