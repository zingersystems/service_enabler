package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.persistence.repository.PaymentRequestRepository;
import com.zingersystms.serviceportal.persistence.repository.ServiceRepository;
import com.zingersystms.serviceportal.services.ReportService;
import com.zingersystms.serviceportal.web.dto.PaymentRequestDTO;
import com.zingersystms.serviceportal.web.dto.ServiceWithNumOfPaidRequestOnlyDTO;
import com.zingersystms.serviceportal.web.dto.SimpleReportDataDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/reports")
@Api(value = "reports", description = "Operations pertaining to reports")
public class ReportController {

    private final
    ServiceRepository serviceRepository;

    private final ReportService reportService;

    private final
    PaymentRequestRepository paymentRequestRepository;

    @Autowired
    public ReportController(ServiceRepository serviceRepository, ReportService reportService, PaymentRequestRepository paymentRequestRepository) {
        this.serviceRepository = serviceRepository;
        this.reportService = reportService;
        this.paymentRequestRepository = paymentRequestRepository;
    }

    @ApiOperation(value = "get default report data", response = SimpleReportDataDTO.class)
    @GetMapping(value = "/default", produces = "application/json")
    @ResponseBody
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    SimpleReportDataDTO getPaymentRequest(
            @RequestParam(defaultValue = "5") int numOfMostRequestServices,
            @RequestParam(defaultValue = "5") int numOfMostPaidServices,
            @RequestParam(defaultValue = "5") int numOfMostProfitableServices,
            @RequestParam(defaultValue = "0") long startDate,
            @RequestParam(defaultValue = "0") long endDate) {

        return this.reportService.getSimpleReportData(numOfMostRequestServices,
                numOfMostPaidServices,
                numOfMostProfitableServices,
                startDate,
                endDate);

    }

    @ApiOperation(value = "Find a payment request by uuid", response = PaymentRequestDTO.class)
    @GetMapping(value = "/test", produces = "application/json")
    @ResponseBody
    Double test() {
        Date startDate = new GregorianCalendar(2017, Calendar.AUGUST, 1).getTime();
        System.out.println(startDate.getTime());
        return this.paymentRequestRepository.findTotalAmount(startDate, new Date());
    }
}
