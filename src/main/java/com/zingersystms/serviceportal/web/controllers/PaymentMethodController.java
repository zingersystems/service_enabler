package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.persistence.model.PaymentMethod;
import com.zingersystms.serviceportal.services.PaymentMethodService;
import com.zingersystms.serviceportal.services.PaymentRequestService;
import com.zingersystms.serviceportal.web.dto.GenericResponse;
import com.zingersystms.serviceportal.web.dto.PaymentMethodDTO;
import com.zingersystms.serviceportal.web.dto.PaymentRequestBareDTO;
import com.zingersystms.serviceportal.web.dto.PaymentRequestDTO;
import com.zingersystms.serviceportal.web.exceptions.PaymentMethodNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.PaymentRequestNotFoundException;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Profile("dev")
public class PaymentMethodController {

    private final PaymentMethodService paymentMethodService;

    @Autowired
    public PaymentMethodController(PaymentMethodService paymentMethodService) {
        this.paymentMethodService = paymentMethodService;
    }

    @ApiOperation(value = "Find a peyment request by uuid", response = PaymentMethodDTO.class)
    @GetMapping(value = "/{uuid}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    PaymentMethodDTO getPaymentMethod(@PathVariable String uuid) {

        return paymentMethodService.findByUuid(uuid);

    }

    @ApiOperation(value = "Create a new payment request", response = PaymentMethodDTO.class)
    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    PaymentMethodDTO createNewPaymentMethod(@RequestBody @Valid PaymentMethodDTO paymentMethodDTO) {

        return paymentMethodService.create(paymentMethodDTO);

    }

    @ApiOperation(value = "find all payment requests", response = PaymentMethodDTO.class)
    @GetMapping(value = "", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Not Authenticated", response = GenericResponse.class)

    })
    Page<PaymentMethodDTO> paymentRequestListPage(Pageable pageable) {

        return paymentMethodService.findAll(pageable);

    }

    @ApiOperation(value = "update payment requests", response = PaymentMethodDTO.class)
    @PutMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    PaymentMethodDTO updatePaymentMethod(@RequestBody @Valid PaymentMethodDTO request) {

        return paymentMethodService.update(request);

    }

    @ApiOperation(value = "delete a payment requests", response = PaymentMethodDTO.class)
    @DeleteMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    PaymentMethodDTO deleteRequest(@PathVariable String id) {

        return paymentMethodService.delete(id);

    }

    @ExceptionHandler(PaymentMethodNotFoundException.class)
    ResponseEntity<GenericResponse> paymentMethodNotFoundException(PaymentMethodNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
