package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.services.StatusService;
import com.zingersystms.serviceportal.web.converters.StatusConverter;
import com.zingersystms.serviceportal.web.dto.StatusDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/status")
@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @GetMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    StatusDTO showStatus(@PathVariable String id) {

        return StatusConverter.convert(statusService.findById(id));

    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    StatusDTO createStatus(@RequestBody @Valid StatusDTO statusDTO) {

        return statusService.create(statusDTO);

    }

    @GetMapping(value = "", produces = "application/json")
    @ResponseBody
    Page<StatusDTO> statusListPage(Pageable pageable) {

        return statusService.findAll(pageable);

    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    StatusDTO editStatus(@RequestBody @Valid StatusDTO statusDTO,
                         @PathVariable String id) {

        return statusService.update(statusDTO);

    }

    @PutMapping(value = "default/{uuid}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    StatusDTO setDefaultStatus(@PathVariable String uuid) {

        return statusService.setDefaultStatus(uuid);

    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    StatusDTO deleteStatus(@PathVariable String id) {
        return statusService.delete(id);
    }

}
