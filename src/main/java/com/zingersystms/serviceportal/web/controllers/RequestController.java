package com.zingersystms.serviceportal.web.controllers;

import com.zingersystms.serviceportal.services.RequestService;
import com.zingersystms.serviceportal.web.converters.RequestConverter;
import com.zingersystms.serviceportal.web.dto.*;
import com.zingersystms.serviceportal.web.exceptions.RequestNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/requests")
@Api(value = "request", description = "Operations pertaining to Requests in Service Portal")
public class RequestController {
    @Autowired
    private RequestService requestService;

    @ApiOperation(value = "Find Request  by id", response = RequestDTO.class)
    @GetMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    RequestDTO showRequest(@PathVariable String id) {

        return RequestConverter.convert(requestService.findById(id));

    }

//    @ApiOperation(value = "Find Request  by id", notes = "", response = RequestDTO.class)
//    @GetMapping(value = "/status/{uuid}", produces = "application/json")
//    @ResponseBody
//    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
//    Page<RequestOnlyDTO> showRequestByStatus(@PathVariable String uuid, Pageable pageable) {
//
//        return this.requestService.findAllByStatusUuid(pageable, uuid);
//
//    }

    @GetMapping(value = "/{id}/comments", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    Page<CommentInRequestDTO> showRequestComments(@PathVariable String id,Pageable pageable) {

        return requestService.getCommentsForRequest(id,pageable);

    }

    @ApiOperation(value = "Create a new Request", response = RequestDTO.class)
    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    RequestDTO createNewRequest(@RequestBody @Valid RequestBareDTO request) {

        return requestService.create(request);

    }

    @ApiOperation(value = "request for service requests with parameters")
    @GetMapping(value = "", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Not Authenticated", response = GenericResponse.class)
    })
    Page<RequestOnlyDTO> requestListPage(Pageable pageable,
                                     @RequestParam(value = "status_uuid", defaultValue = "") String statusUuid,
                                     @RequestParam(value = "start_date", defaultValue = "0") long startDate,
                                     @RequestParam(value = "end_date", defaultValue = "0") long endDate) {

        return requestService.findAllWithParams(pageable, statusUuid, startDate, endDate);

    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    RequestDTO editRequest(@RequestBody @Valid RequestBareDTO request,
                           @PathVariable String id) {

        return requestService.update(request);

    }


    @DeleteMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_USER')")
    RequestDTO deleteRequest(@PathVariable String id) {

        return requestService.delete(id);

    }


    @ExceptionHandler(RequestNotFoundException.class)
    ResponseEntity<GenericResponse> requestNotFoundException(RequestNotFoundException e) {
        GenericResponse error = new GenericResponse(String.valueOf(String.valueOf(HttpStatus.NOT_FOUND)), e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
