package com.zingersystms.serviceportal.web.dto;

import java.util.List;

public class SimpleReportDataDTO {
    private int numOfUsers;
    private int numOfServices;
    private int numOfServiceRequests;
    private int numOfServiceCategories;
    private int numOfPaymentRequests;
    private int numOfPaymentRequestsSettled;
    private double totalIncome;
    private List<ServiceWithNumOfRequestOnlyDTO> mostRequestedServices;
    private List<ServiceWithNumOfPaidRequestOnlyDTO> mostPaidForServices;
//    private List<ServiceWithTotalPaymentDTO> mostProfitableServices;

    public int getNumOfUsers() {
        return numOfUsers;
    }

    public void setNumOfUsers(int numOfUsers) {
        this.numOfUsers = numOfUsers;
    }

    public int getNumOfServices() {
        return numOfServices;
    }

    public void setNumOfServices(int numOfServices) {
        this.numOfServices = numOfServices;
    }

    public int getNumOfServiceRequests() {
        return numOfServiceRequests;
    }

    public void setNumOfServiceRequests(int numOfServiceRequests) {
        this.numOfServiceRequests = numOfServiceRequests;
    }

    public int getNumOfServiceCategories() {
        return numOfServiceCategories;
    }

    public void setNumOfServiceCategories(int numOfServiceCategories) {
        this.numOfServiceCategories = numOfServiceCategories;
    }

    public int getNumOfPaymentRequests() {
        return numOfPaymentRequests;
    }

    public void setNumOfPaymentRequests(int numOfPaymentRequests) {
        this.numOfPaymentRequests = numOfPaymentRequests;
    }

    public int getNumOfPaymentRequestsSettled() {
        return numOfPaymentRequestsSettled;
    }

    public void setNumOfPaymentRequestsSettled(int numOfPaymentRequestsSettled) {
        this.numOfPaymentRequestsSettled = numOfPaymentRequestsSettled;
    }

    public double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public List<ServiceWithNumOfRequestOnlyDTO> getMostRequestedServices() {
        return mostRequestedServices;
    }

    public void setMostRequestedServices(List<ServiceWithNumOfRequestOnlyDTO> mostRequestedServices) {
        this.mostRequestedServices = mostRequestedServices;
    }

    public List<ServiceWithNumOfPaidRequestOnlyDTO> getMostPaidForServices() {
        return mostPaidForServices;
    }

    public void setMostPaidForServices(List<ServiceWithNumOfPaidRequestOnlyDTO> mostPaidForServices) {
        this.mostPaidForServices = mostPaidForServices;
    }

//    public List<ServiceWithTotalPaymentDTO> getMostProfitableServices() {
//        return mostProfitableServices;
//    }
//
//    public void setMostProfitableServices(List<ServiceWithTotalPaymentDTO> mostProfitableServices) {
//        this.mostProfitableServices = mostProfitableServices;
//    }
}
