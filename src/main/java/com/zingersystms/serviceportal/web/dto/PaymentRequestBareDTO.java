package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author kkyb on 8/16/17.
 */
public class PaymentRequestBareDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @ApiModelProperty(required = true)
    private double amount;

    private String motif;

    @ApiModelProperty(required = true)
    private boolean settled;

    @ApiModelProperty(required = true)
    private String requestUuid;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date lastModifiedDate;

    public PaymentRequestBareDTO(){}

    public PaymentRequestBareDTO(PaymentRequest paymentRequest) {
        this.uuid = paymentRequest.getUuid();
        this.amount = paymentRequest.getAmount();
        this.motif = paymentRequest.getMotif();
        this.settled = paymentRequest.isSettled();
        this.requestUuid = paymentRequest.getRequest().getUuid();
        this.createdDate = paymentRequest.getCreatedDate();
        this.lastModifiedDate = paymentRequest.getLastModifiedDate();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public boolean isSettled() {
        return settled;
    }

    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    public String getRequestUuid() {
        return requestUuid;
    }

    public void setRequestUuid(String requestUuid) {
        this.requestUuid = requestUuid;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
