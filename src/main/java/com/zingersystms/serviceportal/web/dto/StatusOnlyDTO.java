package com.zingersystms.serviceportal.web.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel
public class StatusOnlyDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(min = 0, max = 200)
    @NotNull
    @ApiModelProperty(required = true)
    private String name;

    @Size(min = 0, max = 500)
    @NotNull
    @ApiModelProperty(required = true)
    private String description;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date lastModifiedDate;

    public StatusOnlyDTO() {
    }


    public StatusOnlyDTO(Status status) {
        this.uuid = status.getUuid();
        this.name = status.getName();
        this.description = status.getDescription();
        this.createdDate = status.getCreatedDate();
        this.lastModifiedDate = status.getLastModifiedDate();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
