package com.zingersystms.serviceportal.web.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Request;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
@ApiModel
public class RequestBareDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(min = 10, max = 500)
    @ApiModelProperty(required = true)
    private String description;

    @URL
    @NotNull
    private String document;

    private String user;

    private String agent;

    @NotNull
    @ApiModelProperty(required = true)
    private String service;

    private String status;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    public RequestBareDTO() {
    }

    public RequestBareDTO(Request request) {
        this.uuid = request.getUuid();
        this.description = request.getDescription();
        this.document = request.getDocument();
        this.user = request.getCreatedBy().getUuid();
        this.agent = request.getAgentId().getUuid();
        this.createdDate = request.getCreatedDate();
        this.lastModifiedDate = request.getLastModifiedDate();
        this.service = request.getServiceId().getUuid();
        this.status = request.getStatusId().getUuid();
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }
}
