package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Service;
import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import com.zingersystms.serviceportal.web.converters.RequestConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiModel
public class ServiceDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(min = 3, max = 50)
    @NotNull
    private String name;

    @Size(min = 10, max = 500)
    @NotNull
    private String description;

    @URL
    private String photo;

    private List<RequestOnlyDTO> requests;

    @ApiModelProperty(required = true)
    @NotNull
    private ServiceCategoryDTO serviceCategoryDTO;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    public ServiceDTO() {
    }

    public ServiceDTO(Service service) {
        this.uuid = service.getUuid();
        this.name = service.getName();
        this.description = service.getDescription();
        this.photo = service.getPhoto();
        this.requests = RequestConverter.convertOnlyList(service.getRequests());
        this.serviceCategoryDTO = new ServiceCategoryDTO(service.getServiceCategory());
        this.createdDate = service.getCreatedDate();
        this.lastModifiedDate = service.getLastModifiedDate();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<RequestOnlyDTO> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestOnlyDTO> requests) {
        this.requests = requests;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ServiceCategoryDTO getServiceCategoryDTO() {
        return serviceCategoryDTO;
    }

    public void setServiceCategoryDTO(ServiceCategoryDTO serviceCategoryDTO) {
        this.serviceCategoryDTO = serviceCategoryDTO;
    }
}
