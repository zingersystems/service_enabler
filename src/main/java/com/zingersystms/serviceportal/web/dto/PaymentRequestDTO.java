package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.web.converters.RequestConverter;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author kkyb on 8/11/17.
 */
public class PaymentRequestDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @ApiModelProperty(required = true)
    private double amount;

    private String motif;

    @ApiModelProperty(required = true)
    private boolean settled;

    private RequestDTO request;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date lastModifiedDate;

    private String explicitComment;

    public PaymentRequestDTO(){}

    public PaymentRequestDTO(PaymentRequest paymentRequest) {
        this.uuid = paymentRequest.getUuid();
        this.amount = paymentRequest.getAmount();
        this.motif = paymentRequest.getMotif();
        this.settled = paymentRequest.isSettled();
        this.request = RequestConverter.convert(paymentRequest.getRequest());
        this.createdDate = paymentRequest.getCreatedDate();
        this.lastModifiedDate = paymentRequest.getLastModifiedDate();
        this.explicitComment = paymentRequest.getExplicitComment();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public boolean isSettled() {
        return settled;
    }

    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    public RequestDTO getRequest() {
        return request;
    }

    public void setRequest(RequestDTO request) {
        this.request = request;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getExplicitComment() {
        return explicitComment;
    }

    public void setExplicitComment(String explicitComment) {
        this.explicitComment = explicitComment;
    }
}
