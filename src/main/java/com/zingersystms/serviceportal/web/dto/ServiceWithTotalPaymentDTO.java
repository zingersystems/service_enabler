package com.zingersystms.serviceportal.web.dto;

import java.util.Date;

public class ServiceWithTotalPaymentDTO extends ServiceOnlyDTO {
    private double totalPayment;

    public ServiceWithTotalPaymentDTO(String uuid,
                                      String name,
                                      String description,
                                      String photo,
                                      Date createdDate,
                                      Date lastModifiedDate,
                                      String serviceCategoryDTOUuid,
                                      double totalPayment) {

        super(uuid, name, description, photo, serviceCategoryDTOUuid, createdDate, lastModifiedDate);
        this.totalPayment = totalPayment;
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(double totalPayment) {
        this.totalPayment = totalPayment;
    }
}
