package com.zingersystms.serviceportal.web.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.User;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class UserOnlyDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @ApiModelProperty(required = true)
    @Size(max = 50)
    private String username;

    @Email
    @Size(max = 50)
    @ApiModelProperty(required = true)
    private String email;

    private boolean activated;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String photoUrl;

    @NotNull
    private String country;

    @NotNull
    private String phoneNumber;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    public UserOnlyDTO() {
    }

    public UserOnlyDTO(User user) {
        this.uuid = user.getUuid();
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.photoUrl = user.getPhotoUrl();
        this.activated = user.isActivated();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.country = user.getCountry();
        this.phoneNumber = user.getPhoneNumber();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}