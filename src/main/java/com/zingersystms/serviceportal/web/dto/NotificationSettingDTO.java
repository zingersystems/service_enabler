package com.zingersystms.serviceportal.web.dto;

import com.zingersystms.serviceportal.persistence.model.NotificationType;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class NotificationSettingDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @NotNull
    @ApiModelProperty(readOnly = true)
    private NotificationType notificationType;

    @NotNull
    @ApiModelProperty(required = true)
    private boolean activated;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}
