package com.zingersystms.serviceportal.web.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Comment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import java.util.Date;
@ApiModel
public class CommentInRequestDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @NotNull
    private String content;

    @URL
    private String attachmentUrl;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    private UserOnlyDTO createdBy;

    public CommentInRequestDTO() {
    }

    public CommentInRequestDTO(Comment comment) {
        this.uuid = comment.getUuid();
        this.content = comment.getContent();
        this.attachmentUrl = comment.getAttachmentURL();
        this.createdDate = comment.getCreatedDate();
        this.lastModifiedDate = comment.getLastModifiedDate();
        this.createdBy = new UserOnlyDTO(comment.getCreatedBy());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public UserOnlyDTO getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserOnlyDTO createdBy) {
        this.createdBy = createdBy;
    }
}
