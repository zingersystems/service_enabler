package com.zingersystms.serviceportal.web.dto;

public class SimpleReportArgumentDTO {
    private int numOfMostRequestedServices;
    private int numOfMostPaidServices;
    private int numOfMostProfitableServices;
    private long startDate;
    private long endDate;

    public int getNumOfMostRequestedServices() {
        return numOfMostRequestedServices;
    }

    public void setNumOfMostRequestedServices(int numOfMostRequestedServices) {
        this.numOfMostRequestedServices = numOfMostRequestedServices;
    }

    public int getNumOfMostPaidServices() {
        return numOfMostPaidServices;
    }

    public void setNumOfMostPaidServices(int numOfMostPaidServices) {
        this.numOfMostPaidServices = numOfMostPaidServices;
    }

    public int getNumOfMostProfitableServices() {
        return numOfMostProfitableServices;
    }

    public void setNumOfMostProfitableServices(int numOfMostProfitableServices) {
        this.numOfMostProfitableServices = numOfMostProfitableServices;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
