package com.zingersystms.serviceportal.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel
public class PasswordResetDTO {

    @ApiModelProperty(dataType = "String")
    @NotNull
    private String password;
    @ApiModelProperty(required = true)
    @NotNull
    private String token;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
