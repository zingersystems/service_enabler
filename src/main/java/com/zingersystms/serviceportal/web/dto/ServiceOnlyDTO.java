package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Service;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel
public class ServiceOnlyDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @ApiModelProperty(required = true)
    @Size(min = 3, max = 50)
    private String name;

    @ApiModelProperty(required = true)
    @Size(min = 10, max = 500)
    private String description;

    @ApiModelProperty(required = true)
    @URL
    private String photo;

    @ApiModelProperty(required = true)
    @NotNull
    private String serviceCategoryDTOUuid;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    public ServiceOnlyDTO() {
    }

    public ServiceOnlyDTO(Service service) {
        this.uuid = service.getUuid();
        this.name = service.getName();
        this.description = service.getDescription();
        this.photo = service.getPhoto();
        this.serviceCategoryDTOUuid = service.getServiceCategory().getUuid();
        this.createdDate = service.getCreatedDate();
        this.lastModifiedDate = service.getLastModifiedDate();
    }

    public ServiceOnlyDTO(String uuid, String name, String description, String photo, String serviceCategoryDTOUuid, Date createdDate, Date lastModifiedDate) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.serviceCategoryDTOUuid = serviceCategoryDTOUuid;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getServiceCategoryDTOUuid() {
        return serviceCategoryDTOUuid;
    }

    public void setServiceCategoryDTOUuid(String serviceCategoryDTOUuid) {
        this.serviceCategoryDTOUuid = serviceCategoryDTOUuid;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
