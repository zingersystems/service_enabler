package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel
public class ServiceCategoryDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(max = 200)
    @NotNull
    @ApiModelProperty(required = true)
    private String name;

    @Size(max = 500)
    @NotNull
    @ApiModelProperty(required = true)
    private String description;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date lastModifiedDate;

    public ServiceCategoryDTO() {
    }

    public ServiceCategoryDTO(ServiceCategory serviceCategory) {
        this.uuid = serviceCategory.getUuid();
        this.name = serviceCategory.getName();
        this.description = serviceCategory.getDescription();
        this.createdDate = serviceCategory.getCreatedDate();
        this.lastModifiedDate = serviceCategory.getLastModifiedDate();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
