package com.zingersystms.serviceportal.web.dto;

import com.zingersystms.serviceportal.persistence.model.Authorities;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel
public class PermissionUpdateDTO {

    @NotNull
    @ApiModelProperty(required = true)
    private String user_id;

    @NotNull
    @ApiModelProperty(required = true)
    private Authorities authority;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Authorities getAuthority() {
        return authority;
    }

    public void setAuthority(Authorities authority) {
        this.authority = authority;
    }
}
