package com.zingersystms.serviceportal.web.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.web.converters.RequestConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiModel
public class StatusDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(min = 0, max = 200)
    @NotNull
    @ApiModelProperty(required = true)
    private String name;

    @Size(min = 0, max = 500)
    @NotNull
    @ApiModelProperty(required = true)
    private String description;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @ApiModelProperty(readOnly = true)
    private Date lastModifiedDate;

    @ApiModelProperty(hidden = true)
    private List<RequestBareDTO> requests;

    public StatusDTO() {
    }

    public StatusDTO(Status status) {
        this.uuid = status.getUuid();
        this.name = status.getName();
        this.description = status.getDescription();
        this.createdDate = status.getCreatedDate();
        this.lastModifiedDate = status.getLastModifiedDate();
        this.requests = RequestConverter.convertBareList(status.getRequests()); //This line of code generates StackOverFlow exception because of an infinite recursion between RequestConverter.convert() and StatusConverter.convert();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public List<RequestBareDTO> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestBareDTO> requests) {
        this.requests = requests;
    }
}
