package com.zingersystms.serviceportal.web.dto;

import io.swagger.annotations.ApiModelProperty;

public class VpcPaymentInfo {

    @ApiModelProperty(required = true)
    private String paymentRequestUuid;

    @ApiModelProperty(required = true)
    private String refreshToken;

    public String getPaymentRequestUuid() {
        return paymentRequestUuid;
    }

    public void setPaymentRequestUuid(String paymentRequestUuid) {
        this.paymentRequestUuid = paymentRequestUuid;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
