package com.zingersystms.serviceportal.web.dto;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

public class MoMoPaymentInfo {

    @ApiModelProperty(required = true)
    private String paymentRequestUuid;

    @ApiModelProperty(required = true)
    private String userPhoneNumber;

    public String getPaymentRequestUuid() {
        return paymentRequestUuid;
    }

    public void setPaymentRequestUuid(String paymentRequestUuid) {
        this.paymentRequestUuid = paymentRequestUuid;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }
}
