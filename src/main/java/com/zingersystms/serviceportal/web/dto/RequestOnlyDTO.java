package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.web.converters.StatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel
public class RequestOnlyDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(min = 10, max = 500)
    @ApiModelProperty(required = true)
    private String description;

    @URL
    @NotNull
    @ApiModelProperty(notes = "link to document to be processed")
    private String document;

    private UserOnlyDTO user;

    private UserOnlyDTO agent;

    @NotNull
    @ApiModelProperty(required = true)
    private String serviceId;

    private StatusOnlyDTO status;


    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    public RequestOnlyDTO() {
    }

    public RequestOnlyDTO(Request request) {
        this.uuid = request.getUuid();
        this.description = request.getDescription();
        this.document = request.getDocument();
        this.user = new UserOnlyDTO(request.getCreatedBy());
        this.agent = new UserOnlyDTO(request.getAgentId());
        this.createdDate = request.getCreatedDate();
        this.lastModifiedDate = request.getLastModifiedDate();
        this.serviceId = request.getServiceId().getUuid();
        this.status = StatusConverter.convertOnly(request.getStatusId());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public UserOnlyDTO getUser() {
        return user;
    }

    public void setUser(UserOnlyDTO user) {
        this.user = user;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public StatusOnlyDTO getStatus() {
        return status;
    }

    public void setStatus(StatusOnlyDTO status) {
        this.status = status;
    }

    public UserOnlyDTO getAgent() {
        return agent;
    }

    public void setAgent(UserOnlyDTO agent) {
        this.agent = agent;
    }
}
