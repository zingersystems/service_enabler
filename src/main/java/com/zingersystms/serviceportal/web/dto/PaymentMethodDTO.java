package com.zingersystms.serviceportal.web.dto;

import com.zingersystms.serviceportal.persistence.model.PaymentMethod;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Column;

public class PaymentMethodDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @ApiModelProperty(required = true)
    @URL
    private String paymentUrl;

    @ApiModelProperty(required = true)
    @URL
    private String paymentMethodLogo;

    public PaymentMethodDTO () {}

    public PaymentMethodDTO (PaymentMethod paymentMethod) {
        this.uuid = paymentMethod.getUuid();
        this.paymentMethodLogo = paymentMethod.getPaymentMethodLogo();
        this.paymentUrl = paymentMethod.getPaymentUrl();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    public String getPaymentMethodLogo() {
        return paymentMethodLogo;
    }

    public void setPaymentMethodLogo(String paymentMethodLogo) {
        this.paymentMethodLogo = paymentMethodLogo;
    }
}
