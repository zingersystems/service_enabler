package com.zingersystms.serviceportal.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.web.converters.CommentConverter;
import com.zingersystms.serviceportal.web.converters.StatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiModel
public class RequestDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @ApiModelProperty(required = true)
    @Size(min = 10, max = 500)
    private String description;

    @URL
    @Size(min = 5)
    @ApiModelProperty(notes = "link to docummet to be processed")
    private String document;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date lastModifiedDate;

    private ServiceOnlyDTO service;

    private UserOnlyDTO user;

    private UserOnlyDTO agent;

    private StatusOnlyDTO status;

    private List<CommentInRequestDTO> comments;

    public RequestDTO() {
    }

    public RequestDTO(Request request) {
        this.uuid = request.getUuid();
        this.description = request.getDescription();
        this.document = request.getDocument();
        this.service = new ServiceOnlyDTO(request.getServiceId());
        this.user = new UserOnlyDTO(request.getCreatedBy());
        this.agent = new UserOnlyDTO(request.getAgentId());
        this.status = StatusConverter.convertOnly(request.getStatusId());
        this.createdDate = request.getCreatedDate();
        this.lastModifiedDate = request.getLastModifiedDate();
        this.comments = CommentConverter.convert(request.getComments());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public ServiceOnlyDTO getService() {
        return service;
    }

    public void setService(ServiceOnlyDTO service) {
        this.service = service;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UserOnlyDTO getUser() {
        return user;
    }

    public void setUser(UserOnlyDTO user) {
        this.user = user;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public UserOnlyDTO getAgent() {
        return agent;
    }

    public void setAgent(UserOnlyDTO agent) {
        this.agent = agent;
    }

    public List<CommentInRequestDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentInRequestDTO> comments) {
        this.comments = comments;
    }

    public StatusOnlyDTO getStatus() {
        return status;
    }

    public void setStatus(StatusOnlyDTO status) {
        this.status = status;
    }
}
