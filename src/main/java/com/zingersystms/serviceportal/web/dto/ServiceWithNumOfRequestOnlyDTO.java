package com.zingersystms.serviceportal.web.dto;

import java.util.Date;

public class ServiceWithNumOfRequestOnlyDTO extends ServiceOnlyDTO {
    private int numOfRequests;

    public ServiceWithNumOfRequestOnlyDTO(String uuid,
                                          String name,
                                          String description,
                                          String photo,
                                          Date createdDate,
                                          Date lastModifiedDate,
                                          String serviceCategoryDTOUuid,
                                          int numOfRequests) {

        super(uuid, name, description, photo, serviceCategoryDTOUuid, createdDate, lastModifiedDate);
        this.numOfRequests = numOfRequests;
    }

    public int getNumOfRequests() {
        return numOfRequests;
    }
}
