package com.zingersystms.serviceportal.web.dto;

import java.util.Date;

public class ServiceWithNumOfPaidRequestOnlyDTO extends ServiceOnlyDTO {
    private int numOfPaidRequests;

    public ServiceWithNumOfPaidRequestOnlyDTO(String uuid,
                                          String name,
                                          String description,
                                          String photo,
                                          Date createdDate,
                                          Date lastModifiedDate,
                                          String serviceCategoryDTOUuid,
                                          int numOfPaidRequests) {

        super(uuid, name, description, photo, serviceCategoryDTOUuid, createdDate, lastModifiedDate);
        this.numOfPaidRequests = numOfPaidRequests;
    }

    public int getNumOfPaidRequests() {
        return numOfPaidRequests;
    }

    public void setNumOfPaidRequests(int numOfPaidRequests) {
        this.numOfPaidRequests = numOfPaidRequests;
    }
}
