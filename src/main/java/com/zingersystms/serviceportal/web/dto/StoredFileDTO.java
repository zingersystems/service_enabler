package com.zingersystms.serviceportal.web.dto;

import com.zingersystms.serviceportal.persistence.model.StoredFile;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;

@ApiModel
public class StoredFileDTO {

    @ApiModelProperty(readOnly = true)
    private String uuid;

    @Size(min = 3, max = 50)
    @ApiModelProperty(required = true)
    private String filename;

    @ApiModelProperty(readOnly = true)
    private long size;

    @ApiModelProperty(readOnly = true)
    private String fileUrl;

    public StoredFileDTO() {
    }

    public StoredFileDTO(String uuid, String filename, long size, String fileUrl) {
        this.uuid = uuid;
        this.filename = filename;
        this.size = size;
        this.fileUrl = fileUrl;
    }

    public StoredFileDTO(StoredFile storedFile) {
        this.uuid = storedFile.getUuid();
        this.filename = storedFile.getFilename();
        this.size = storedFile.getSize();
        this.fileUrl = storedFile.getFileUrl();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
