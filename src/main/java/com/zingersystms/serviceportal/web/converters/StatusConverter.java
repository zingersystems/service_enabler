package com.zingersystms.serviceportal.web.converters;


import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.web.dto.StatusDTO;
import com.zingersystms.serviceportal.web.dto.StatusOnlyDTO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StatusConverter {
    public static List<StatusDTO> convert(List<Status> status) {
        return status.stream().map(StatusDTO::new)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

    }

    public static StatusDTO convert(Status status) {
        return new StatusDTO(status);

    }

    public static StatusOnlyDTO convertOnly(Status statusId) {
        return new StatusOnlyDTO(statusId);
    }
}
