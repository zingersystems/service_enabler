package com.zingersystms.serviceportal.web.converters;

import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.web.dto.RequestBareDTO;
import com.zingersystms.serviceportal.web.dto.RequestDTO;
import com.zingersystms.serviceportal.web.dto.RequestOnlyDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RequestConverter {


    public static List<RequestDTO> convert(List<Request> requestList) {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        try {

            User user = ((User) authentication.getPrincipal());
            boolean roleAdmin = user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));

            try {
                return requestList.stream().map(request -> {
                    if (request.getCreatedBy().getUsername().equals(user.getUsername()) || roleAdmin) {

                        return new RequestDTO(request);
                    }
                    return null;
                }).filter(Objects::nonNull)
                        .collect(Collectors.toList());
            } catch (NullPointerException e) {
                return null;
            }

        } catch (ClassCastException e) {
            return null;
        }

    }

    public static RequestDTO convert(Request request) {
        return new RequestDTO(request);
    }

    public static RequestOnlyDTO convertOnly(Request request) {
        return new RequestOnlyDTO(request);
    }

    public static List<RequestOnlyDTO> convertOnlyList(List<Request> requests) {

        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        try {

            User user = ((User) authentication.getPrincipal());
            boolean roleAdmin = user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));

            try {
                return requests.stream().map(request -> {
                    if (request.getCreatedBy().getUsername().equals(user.getUsername()) || roleAdmin) {

                        return new RequestOnlyDTO(request);
                    }
                    return null;
                }).filter(Objects::nonNull)
                        .collect(Collectors.toList());
            } catch (NullPointerException e) {
                return null;
            }

        } catch (ClassCastException e) {
            return null;
        }

    }

    public static List<RequestBareDTO> convertBareList(List<Request> requests) {

        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        try {

            User user = ((User) authentication.getPrincipal());
            boolean roleAdmin = user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));

            try {
                return requests.stream().map(request -> {
                    if (request.getCreatedBy().getUsername().equals(user.getUsername()) || roleAdmin) {

                        return new RequestBareDTO(request);
                    }
                    return null;
                }).filter(Objects::nonNull)
                        .collect(Collectors.toList());
            } catch (NullPointerException e) {
                return null;
            }

        } catch (ClassCastException e) {
            return null;
        }
    }
}
