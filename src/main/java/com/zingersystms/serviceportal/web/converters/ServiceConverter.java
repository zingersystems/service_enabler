package com.zingersystms.serviceportal.web.converters;


import com.zingersystms.serviceportal.persistence.model.Service;
import com.zingersystms.serviceportal.web.dto.ServiceDTO;
import com.zingersystms.serviceportal.web.dto.ServiceOnlyDTO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ServiceConverter {

    public static List<ServiceDTO> convert(List<Service> services) {
        return services.stream().map(ServiceDTO::new)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

    }

    public static List<ServiceOnlyDTO> convertOnly(List<Service> services) {
        return services.stream().map(ServiceOnlyDTO::new)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static ServiceDTO convert(Service service) {
        return new ServiceDTO(service);

    }

    public static ServiceOnlyDTO convertOnly(Service service) {
        return new ServiceOnlyDTO(service);
    }
}
