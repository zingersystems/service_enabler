package com.zingersystms.serviceportal.web.converters;

import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import com.zingersystms.serviceportal.web.dto.ServiceCategoryDTO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ServiceCategoryConverter {

    public static List<ServiceCategoryDTO> convert(List<ServiceCategory> serviceCategories) {
        return serviceCategories.stream().map(ServiceCategoryDTO::new)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

    }
}
