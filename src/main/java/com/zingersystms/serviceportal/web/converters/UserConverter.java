package com.zingersystms.serviceportal.web.converters;


import com.zingersystms.serviceportal.persistence.model.User;
import com.zingersystms.serviceportal.web.dto.UserDTO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserConverter {

    public static List<UserDTO> convert(List<User> users) {
        return users.stream().map(UserDTO::new)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

    }

    public static UserDTO convert(User user) {
        return new UserDTO(user);
    }
}
