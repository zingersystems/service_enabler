package com.zingersystms.serviceportal.web.converters;

import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.web.dto.CommentInRequestDTO;
import com.zingersystms.serviceportal.web.dto.CommentOnlyDTO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CommentConverter {
    public static CommentOnlyDTO convert(Comment comment) {
        return new CommentOnlyDTO(comment);
    }

    public static List<CommentInRequestDTO> convert(List<Comment> comments) {
        if (comments != null) {
            return comments.stream().map(CommentInRequestDTO::new)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        return null;
    }
}
