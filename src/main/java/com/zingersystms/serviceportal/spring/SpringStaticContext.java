package com.zingersystms.serviceportal.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringStaticContext implements ApplicationContextAware {

    private static ApplicationContext context;

    @Autowired
    public SpringStaticContext(ApplicationContext applicationContext) {
        context = applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        applicationContext = applicationContext;
    }

    public static ApplicationContext getAppContext() {
        return context;
    }
}
