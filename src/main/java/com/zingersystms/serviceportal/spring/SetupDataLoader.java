package com.zingersystms.serviceportal.spring;

import com.zingersystms.serviceportal.persistence.model.*;
import com.zingersystms.serviceportal.persistence.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;
    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    private final StatusRepository statusRepository;

    private final PasswordEncoder passwordEncoder;

    private final NotificationSettingRepository notificationSettingRepository;

    private final DefaultValueRepository defaultValueRepository;

    @Autowired
    public SetupDataLoader(UserRepository userRepository,
                           NotificationSettingRepository notificationSettingRepository,
                           AuthorityRepository authorityRepository,
                           StatusRepository statusRepository,
                           PasswordEncoder passwordEncoder, DefaultValueRepository defaultValueRepository) {

        this.userRepository = userRepository;
        this.notificationSettingRepository = notificationSettingRepository;
        this.authorityRepository = authorityRepository;
        this.statusRepository = statusRepository;
        this.passwordEncoder = passwordEncoder;
        this.defaultValueRepository = defaultValueRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        setUpDefaultAdmin();

        setUpDefaultUser();

        setUpDefaultStatus();

        setupDefaultNotificationSettings();

        alreadySetup = true;
    }

    private void setUpDefaultStatus() {
        DefaultValue defaultValue = defaultValueRepository
                .findByKeyForDefaultValue(KeyForDefaultValue.DEFAULT_REQUEST_STATUS);
        if(defaultValue == null || statusRepository.findByUuid(defaultValue.getDefaultValueReference()) == null) {
            Status status = new Status();
            status.setName("Pending");
            status.setDescription("Specifies that a request is in the pending state");
            status = statusRepository.save(status);
            if(defaultValue == null) {
                defaultValueRepository.save(new DefaultValue(KeyForDefaultValue.DEFAULT_REQUEST_STATUS, status.getUuid()));
            }else {
                defaultValue.setDefaultValueReference(status.getUuid());
                defaultValueRepository.save(defaultValue);
            }
        }

    }

    private void setUpDefaultAdmin() {

        final User user = new User();
        user.setUsername("test");
        user.setPassword(passwordEncoder.encode("test"));
        user.setActivated(true);
        user.setEmail("se@zingersystems.com");
        user.setFirstName("test");
        user.setLastName("test");
        user.setPhoneNumber("677777777");
        user.setCountry("Cameroon");

        user.setAuthorities(Arrays.asList(createRoleIfNotFound("ROLE_ADMIN"), createRoleIfNotFound("ROLE_USER")));

        if (userRepository.findByEmailAndDeletedFalse(user.getEmail()) == null) {
            userRepository.save(user);
        }

    }

    private void setUpDefaultUser() {

        final User user = new User();
        user.setUsername("agent");
        user.setPassword(passwordEncoder.encode("agent"));
        user.setEmail("agent@agent.com");
        user.setFirstName("agent");
        user.setLastName("agent");
        user.setPhoneNumber("677777776");
        user.setCountry("Cameroon");
        if (userRepository.findByEmailAndDeletedFalse(user.getEmail()) == null) {
            userRepository.save(user);
        }
    }

    private Authority createRoleIfNotFound(String roleName) {
        Authority authority = authorityRepository.findOne(roleName);
        if (authority == null) {
            authority = new Authority(roleName);
            authorityRepository.save(authority);
        }
        return authority;
    }

    private void setupDefaultNotificationSettings() {

        saveNotificationSettingIfNotExist(NotificationType.ON_REQUEST_STATUS_CHANGED);
        saveNotificationSettingIfNotExist(NotificationType.ON_REQUEST_CREATED);
        saveNotificationSettingIfNotExist(NotificationType.ON_PAYMENT_REQUESTED);
        saveNotificationSettingIfNotExist(NotificationType.ON_SERVICE_CREATED);
        saveNotificationSettingIfNotExist(NotificationType.ON_COMMENT_FOR_REQUEST_DONE);
    }

    private void saveNotificationSettingIfNotExist(NotificationType notificationType) {
        if(this.notificationSettingRepository.findByNotificationType(notificationType) == null){
            NotificationSetting onRequestStatusChanged =
                    new NotificationSetting(notificationType, true);
            this.notificationSettingRepository.save(onRequestStatusChanged);
        }
    }
}
