package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.UserPreference;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPreferenceRepository extends JpaRepository<UserPreference, Long> {
}
