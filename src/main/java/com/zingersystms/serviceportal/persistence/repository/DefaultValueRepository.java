package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.DefaultValue;
import com.zingersystms.serviceportal.persistence.model.KeyForDefaultValue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DefaultValueRepository extends JpaRepository<DefaultValue, Long> {
    DefaultValue findByKeyForDefaultValue(KeyForDefaultValue keyForDefaultValue);
}
