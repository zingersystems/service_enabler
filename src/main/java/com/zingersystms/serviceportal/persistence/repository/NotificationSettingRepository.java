package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.NotificationSetting;
import com.zingersystms.serviceportal.persistence.model.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationSettingRepository extends JpaRepository<NotificationSetting, Integer> {
    NotificationSetting findByNotificationType(NotificationType notificationType);
    boolean findActivatedByNotificationType(NotificationType notificationType);
}
