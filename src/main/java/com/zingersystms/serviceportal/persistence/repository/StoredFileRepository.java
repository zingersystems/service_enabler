package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.StoredFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoredFileRepository extends JpaRepository<StoredFile, Integer> {
    StoredFile findByUuid(String uuid);
}
