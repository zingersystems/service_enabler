package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.Date;

public interface RequestRepository extends RevisionRepository<Request, Long, Integer>, JpaRepository<Request, Long> {
    Request findByUuid(String uuid);
    int countAllByCreatedDateBetween(Date startDate, Date endDate);
    Page<Request> findAllByStatusId(Status status, Pageable pageable);
    Page<Request> findAllByCreatedDateBetween(Pageable pageable, Date startDate, Date endDate);
    Page<Request> findAllByStatusIdAndCreatedDateBetween(Pageable pageable, Status status, Date startDate, Date endDate);
}
