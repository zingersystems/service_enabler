package com.zingersystms.serviceportal.persistence.repository;


import com.zingersystms.serviceportal.persistence.model.Comment;
import com.zingersystms.serviceportal.persistence.model.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

    Comment findByUuid(String uuid);

    Page<Comment> findByRequest(Request request, Pageable pageable);

}
