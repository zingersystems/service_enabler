package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface ServiceCategoryRepository extends JpaRepository<ServiceCategory, Long> {
    ServiceCategory findByUuid(String uuid);
    int countAllByCreatedDateBetween(Date startDate, Date endDate);
}
