package com.zingersystms.serviceportal.persistence.repository;


import com.zingersystms.serviceportal.persistence.model.Request;
import com.zingersystms.serviceportal.persistence.model.Service;
import com.zingersystms.serviceportal.persistence.model.ServiceCategory;
import com.zingersystms.serviceportal.persistence.model.Status;
import com.zingersystms.serviceportal.web.dto.ServiceWithTotalPaymentDTO;
import com.zingersystms.serviceportal.web.dto.ServiceWithNumOfPaidRequestOnlyDTO;
import com.zingersystms.serviceportal.web.dto.ServiceWithNumOfRequestOnlyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ServiceRepository extends JpaRepository<Service, Integer> {
    Service findByUuid(String uuid);
    List<ServiceWithNumOfRequestOnlyDTO> getMostRequestedServices(Date startDate, Date endDate);
    List<ServiceWithNumOfPaidRequestOnlyDTO> getMostPaidForServices(Date startDate, Date endDate);
    int countAllByCreatedDateBetween(Date startDate, Date endDate);

    Page<Service> findAllByCreatedDateBetween(Pageable pageable, Date startDate, Date endDate);

    Page<Service> findAllByServiceCategoryAndCreatedDateBetween(Pageable pageable, ServiceCategory serviceCategory,
                                                         Date startDate, Date endDate);
//    List<ServiceWithTotalPaymentDTO> getMostProfitableServices();
}
