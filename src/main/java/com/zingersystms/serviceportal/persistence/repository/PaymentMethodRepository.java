package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long>{
    PaymentMethod findByUuid(String uuid);
}
