package com.zingersystms.serviceportal.persistence.repository;


import com.zingersystms.serviceportal.persistence.model.Authority;
import com.zingersystms.serviceportal.persistence.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsernameIgnoreCaseAndDeletedFalse(String username);

    User findByUuidAndDeletedFalse(String uuid);

    User findByEmailAndDeletedFalse(String email);

    User findByPhoneNumberAndDeletedFalse(String phoneNumber);

    int countByAuthoritiesIsNotAndActivatedTrueAndDeletedFalse(Authority authority);

    int countByAuthoritiesIsNotAndActivatedTrueAndDeletedFalseAndCreatedDateBetween(Authority authority,
                                                                                    Date startDate,
                                                                                    Date endDate);

    Page<User> findByDeletedFalse(Pageable pageable);

}