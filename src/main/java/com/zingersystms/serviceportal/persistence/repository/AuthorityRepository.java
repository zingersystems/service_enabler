package com.zingersystms.serviceportal.persistence.repository;


import com.zingersystms.serviceportal.persistence.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, String> {

}
