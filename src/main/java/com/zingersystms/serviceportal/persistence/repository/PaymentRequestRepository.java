package com.zingersystms.serviceportal.persistence.repository;

import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.Date;

public interface PaymentRequestRepository extends RevisionRepository<PaymentRequest, Long, Integer>,
        JpaRepository<PaymentRequest, Long> {

    PaymentRequest findByUuid(String uuid);
    Page<PaymentRequest> findByRequestCreatedByUsername(String userName, Pageable pageable);
    int countAllBySettledTrueAndLastModifiedDateIsBetween(Date startDate, Date endDate);

    int countAllByCreatedDateBetween(Date startDate, Date endDate);

    @Query(value = "select sum(amount) " +
            "from payment_request " +
            "where payment_request.settled=TRUE " +
            "and payment_request.last_modified_date between ? and ?", nativeQuery = true)
    Double findTotalAmount(Date startDate, Date endDate);
}
