package com.zingersystms.serviceportal.persistence.repository;


import com.zingersystms.serviceportal.persistence.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Long> {

    Status findByUuid(String uuid);
    Status findByName(String name);
}
