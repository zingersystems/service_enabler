package com.zingersystms.serviceportal.persistence.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class DefaultValue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false, updatable = false)
    private String uuid;

    @Column(nullable = false, unique = true)
    @Enumerated(EnumType.STRING)
    private KeyForDefaultValue keyForDefaultValue;

    private String defaultValueReference;

    public DefaultValue(){}

    public DefaultValue(KeyForDefaultValue keyForDefaultValue, String defaultValueReference) {
        this.keyForDefaultValue = keyForDefaultValue;
        this.defaultValueReference = defaultValueReference;
    }

    @PrePersist
    private void prePersist() {
        this.uuid= UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public KeyForDefaultValue getKeyForDefaultValue() {
        return keyForDefaultValue;
    }

    public void setKeyForDefaultValue(KeyForDefaultValue keyForDefaultValue) {
        this.keyForDefaultValue = keyForDefaultValue;
    }

    public String getDefaultValueReference() {
        return defaultValueReference;
    }

    public void setDefaultValueReference(String defaultValueReference) {
        this.defaultValueReference = defaultValueReference;
    }
}
