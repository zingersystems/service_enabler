package com.zingersystms.serviceportal.persistence.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.web.dto.ServiceWithNumOfPaidRequestOnlyDTO;
import com.zingersystms.serviceportal.web.dto.ServiceWithNumOfRequestOnlyDTO;
import com.zingersystms.serviceportal.web.dto.ServiceWithTotalPaymentDTO;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@NamedNativeQueries(
        {
                @NamedNativeQuery(
                        name="Service.getMostRequestedServices",
                        query="SELECT service.uuid, " +
                                "service.name, " +
                                "service.description, " +
                                "service.photo, " +
                                "service.created_date," +
                                "service.last_modified_date," +
                                "service_category.uuid AS service_category_uuid," +
                                "rsi.num_of_request " +
                                "FROM (SELECT request.service_id, COUNT(*) AS num_of_request " +
                                    "FROM request " +
                                    "WHERE request.created_date BETWEEN ? AND ?" +
                                    "GROUP BY request.service_id) " +
                                "AS rsi, service, service_category " +
                                "WHERE rsi.service_id=service.id " +
                                "AND service.service_category_id = service_category.id " +
                                "ORDER BY num_of_request DESC " +
                                "LIMIT 5",

                        resultSetMapping="MostRequestedServicesMapping"
                ),
                @NamedNativeQuery(
                        name="Service.getMostPaidForServices",
                        query="SELECT\n" +
                                "  service.uuid,\n" +
                                "  service.name,\n" +
                                "  service.description,\n" +
                                "  service.photo,\n" +
                                "  service.created_date,\n" +
                                "  service.last_modified_date,\n" +
                                "  service.uuid AS service_category_uuid,\n" +
                                "  SUM(rsi.num_of_payment) AS total_num_of_payment\n" +
                                "FROM (SELECT\n" +
                                "        request.service_id,\n" +
                                "        COUNT(*) AS num_of_payment\n" +
                                "      FROM payment_request, request\n" +
                                "      WHERE payment_request.settled = TRUE " +
                                "      AND payment_request.request_id = request.id\n" +
                                "      AND request.created_date BETWEEN ? AND ?\n" +
                                "      GROUP BY payment_request.request_id) AS rsi, service\n" +
                                "WHERE rsi.service_id = service.id\n" +
                                "GROUP BY service.id\n" +
                                "ORDER BY total_num_of_payment DESC " +
                                "LIMIT 5",

                        resultSetMapping="MostPaidForServicesMapping"
                )
//                ,
//                @NamedNativeQuery(
//                        name="Service.getMostProfitableServices",
//                        query="SELECT\n" +
//                                "  service.uuid,\n" +
//                                "  service.name,\n" +
//                                "  service.description,\n" +
//                                "  service.photo,\n" +
//                                "  service.created_date,\n" +
//                                "  service.last_modified_date,\n" +
//                                "  service.uuid AS service_category_uuid,\n" +
//                                "  SUM(rsi.num_of_payment) AS total_payment\n" +
//                                "FROM (SELECT\n" +
//                                "        request.service_id,\n" +
//                                "        SUM(payment_request.amount) AS num_of_payment\n" +
//                                "      FROM payment_request, request\n" +
//                                "      WHERE payment_request.settled = TRUE AND payment_request.request_id = request.id\n" +
//                                "      GROUP BY payment_request.request_id) AS rsi, service\n" +
//                                "WHERE rsi.service_id = service.id\n" +
//                                "GROUP BY service.id\n" +
//                                "ORDER BY total_payment DESC \n" +
//                                "LIMIT 5",
//
//                        resultSetMapping="MostProfitableServicesMapping"
//                )
        }
)
@SqlResultSetMappings(
        {
                @SqlResultSetMapping(
                        name = "MostRequestedServicesMapping",
                        classes = {@ConstructorResult(
                                targetClass = ServiceWithNumOfRequestOnlyDTO.class,
                                columns = { @ColumnResult(name = "uuid", type = String.class),
                                        @ColumnResult(name = "name", type = String.class),
                                        @ColumnResult(name = "description", type = String.class),
                                        @ColumnResult(name = "photo", type = String.class),
                                        @ColumnResult(name = "created_date", type = Date.class),
                                        @ColumnResult(name = "last_modified_date", type = Date.class),
                                        @ColumnResult(name = "service_category_uuid", type = String.class),
                                        @ColumnResult(name = "num_of_request", type = Integer.class)
                                }
                        )}
                ),
                @SqlResultSetMapping(
                        name = "MostPaidForServicesMapping",
                        classes = { @ConstructorResult(
                                targetClass = ServiceWithNumOfPaidRequestOnlyDTO.class,
                                columns = { @ColumnResult(name = "uuid", type = String.class),
                                        @ColumnResult(name = "name", type = String.class),
                                        @ColumnResult(name = "description", type = String.class),
                                        @ColumnResult(name = "photo", type = String.class),
                                        @ColumnResult(name = "created_date", type = Date.class),
                                        @ColumnResult(name = "last_modified_date", type = Date.class),
                                        @ColumnResult(name = "service_category_uuid", type = String.class),
                                        @ColumnResult(name = "total_num_of_payment", type = Integer.class)
                                }
                        )}
                )
//                ,
//                @SqlResultSetMapping(
//                        name = "MostProfitableServicesMapping",
//                        classes = {
//                                @ConstructorResult(
//                                        targetClass = ServiceWithTotalPaymentDTO.class,
//                                        columns = { @ColumnResult(name = "uuid", type = String.class),
//                                                @ColumnResult(name = "name", type = String.class),
//                                                @ColumnResult(name = "description", type = String.class),
//                                                @ColumnResult(name = "photo", type = String.class),
//                                                @ColumnResult(name = "created_date", type = Date.class),
//                                                @ColumnResult(name = "last_modified_date", type = Date.class),
//                                                @ColumnResult(name = "service_category_uuid", type = String.class),
//                                                @ColumnResult(name = "total_payment", type = Double.class)
//                                        }
//                                )
//                        }
//                )
        }
)

public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false, updatable = false)
    private String uuid;

    @Size(min = 3, max = 50)
    @Column(nullable = false)
    private String name;

    @Lob
    @Size(min = 10, max = 500)
    @Column(nullable = false)
    private String description;

    @Lob
    @URL
    @Column(nullable = false)
    private String photo;

    @OneToMany(mappedBy = "serviceId", fetch = FetchType.LAZY)
    private List<Request> requests;

    @Column(nullable = false, updatable = false)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @Column(nullable = false)
    private Date lastModifiedDate;

    @ManyToOne
    @JoinColumn(name = "service_category_id")
    @NotNull
    private ServiceCategory serviceCategory;

    @PrePersist
    private void prePersist() {
        this.createdDate = new Date();
        this.lastModifiedDate = new Date();
        this.uuid= UUID.randomUUID().toString();

    }

    @PreUpdate
    private void preUpdate() {
        this.lastModifiedDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdAt) {
        this.createdDate = createdAt;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date updatedAt) {
        this.lastModifiedDate = updatedAt;
    }

    public ServiceCategory getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(ServiceCategory serviceCategory) {
        this.serviceCategory = serviceCategory;
    }
}
