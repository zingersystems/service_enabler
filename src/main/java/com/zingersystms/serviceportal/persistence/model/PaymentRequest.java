package com.zingersystms.serviceportal.persistence.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Entity
public class PaymentRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false, updatable = false)
    private String uuid;

    @JoinColumn(name = "request_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Request request;

    @Column(nullable = false)
    private double amount;

    private String motif;

    @Column(nullable = false)
    private boolean settled;

    @Column(nullable = false, updatable = false)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @Column(nullable = false)
    private Date lastModifiedDate;

    @Column
    private String shortComment;

    @Column
    private String explicitComment;

    @Column
    @ColumnDefault("0")
    private int numberOfPaymentTries;

    @PrePersist
    private void prePersist() {
        this.createdDate = new Date();
        this.lastModifiedDate = new Date();
        this.uuid= UUID.randomUUID().toString();
    }

    @PreUpdate
    private void preUpdate() {
        setLastModifiedDate(new Date());
    }

    public PaymentRequest(){}




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public boolean isSettled() {
        return settled;
    }

    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getShortComment() {
        return shortComment;
    }

    public void setShortComment(String shortComment) {
        this.shortComment = shortComment;
    }

    public String getExplicitComment() {
        return explicitComment;
    }

    public void setExplicitComment(String explicitComment) {
        this.explicitComment = explicitComment;
    }

    public int getNumberOfPaymentTries() {
        return numberOfPaymentTries;
    }

    public void setNumberOfPaymentTries(int numberOfPaymentTries) {
        this.numberOfPaymentTries = numberOfPaymentTries;
    }
}
