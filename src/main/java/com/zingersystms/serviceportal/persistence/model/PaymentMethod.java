package com.zingersystms.serviceportal.persistence.model;

import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
public class PaymentMethod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false, updatable = false)
    private String uuid;

    @Column(nullable = false)
    @URL
    private String paymentUrl;

    @Column(nullable = false)
    @URL
    private String paymentMethodLogo;

    @PrePersist
    private void prePersist() {
        this.uuid= UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    public String getPaymentMethodLogo() {
        return paymentMethodLogo;
    }

    public void setPaymentMethodLogo(String paymentMethodLogo) {
        this.paymentMethodLogo = paymentMethodLogo;
    }
}
