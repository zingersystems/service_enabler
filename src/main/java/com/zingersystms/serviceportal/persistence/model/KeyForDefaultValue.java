package com.zingersystms.serviceportal.persistence.model;

public enum KeyForDefaultValue {
    DEFAULT_ADMIN,
    DEFAULT_USER,
    DEFAULT_REQUEST_STATUS
}
