package com.zingersystms.serviceportal.persistence.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zingersystms.serviceportal.persistence.repository.UserPreferenceRepository;
import com.zingersystms.serviceportal.spring.SpringStaticContext;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.Email;
import org.springframework.context.ApplicationContext;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false, updatable = false)
    private String uuid;

    @Column(nullable = false, unique = true)
    @Size( max = 50)
    @NotNull
    private String username;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    private String photoUrl;

    @Size(max = 500)
    @NotNull
    @Column(nullable = false)
    private String password;

    @Email
    @NotNull
    @Size(max = 50)
    @Column(nullable = false,unique = true)
    private String email;

    @NotNull
    @Size(min=3, max=30)
    @Column(nullable=false)
    private String country;

    @NotNull
    @Size(min=5, max=20)
    @Column(nullable = false, unique = true)
    private String phoneNumber;

    @ColumnDefault("false")
    private boolean deleted;

    private boolean activated;

    @ApiModelProperty(hidden = true)
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Request> requests;

    @ApiModelProperty(hidden = true)
    @ManyToMany(targetEntity = Authority.class)
    @JoinTable(
            name = "user_authority",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority"))
    private List<Authority> authorities;

    @OneToOne(targetEntity = UserPreference.class)
    private UserPreference userPreference;

    @ApiModelProperty(readOnly = true)
    @CreatedDate
    @Column(nullable = false, updatable = false)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    private Date createdDate;

    @ApiModelProperty(readOnly = true)
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @Column(nullable = false)
    private Date lastModifiedDate;

    @PrePersist
    private void prePersist() {
        this.createdDate = new Date();
        this.lastModifiedDate = new Date();
        this.uuid= UUID.randomUUID().toString();
        UserPreference userPreference = new UserPreference();
        userPreference.setEmailNotification(true);
        ApplicationContext context = SpringStaticContext.getAppContext();
        UserPreferenceRepository userPreferenceRepository = (UserPreferenceRepository)context.getBean("userPreferenceRepository");
        userPreference = userPreferenceRepository.save(userPreference);
        this.userPreference = userPreference;
    }

    @PreUpdate
    private void preUpdate() {
        this.lastModifiedDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdAt) {
        this.createdDate = createdAt;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date updatedAt) {
        this.lastModifiedDate = updatedAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public UserPreference getUserPreference() {
        return userPreference;
    }

    public void setUserPreference(UserPreference userPreference) {
        this.userPreference = userPreference;
    }

    public static org.springframework.security.core.userdetails.User getSpringCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        return (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return uuid.equals(user.uuid);

    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
