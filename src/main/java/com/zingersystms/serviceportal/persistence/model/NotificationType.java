package com.zingersystms.serviceportal.persistence.model;

public enum NotificationType {
    ON_REQUEST_STATUS_CHANGED,
    ON_REQUEST_CREATED,
    ON_PAYMENT_REQUESTED,
    ON_SERVICE_CREATED,
    ON_COMMENT_FOR_REQUEST_DONE
}
