package com.zingersystms.serviceportal.persistence.model;


public enum Authorities {
    ROLE_ANONYMOUS,
    ROLE_USER,
    ROLE_ADMIN

}
