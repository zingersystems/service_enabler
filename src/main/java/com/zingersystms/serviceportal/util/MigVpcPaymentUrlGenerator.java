package com.zingersystms.serviceportal.util;

import com.zingersystms.serviceportal.persistence.model.PaymentRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
public class MigVpcPaymentUrlGenerator {

    private static final String VPC_ACCESS_CODE_FIELD_NAME = "vpc_AccessCode";
    private static final String VPC_AMOUNT_FIELD_NAME = "vpc_Amount";
    private static final String VPC_COMMAND_FIELD_NAME = "vpc_Command";
    private static final String VPC_LOCALE_FIELD_NAME = "vpc_Locale";
    private static final String VPC_MERCHANT_FIELD_NAME = "vpc_Merchant";
    private static final String VPC_MERCHTXREF_FIELD_NAME = "vpc_MerchTxnRef";
    private static final String VPC_ORDER_INFO_FIELD_NAME = "vpc_OrderInfo";
    private static final String VPC_RETURN_URL_FIELD_NAME = "vpc_ReturnURL";
    private static final String VPC_SECURE_HASH_FIELD_NAME = "vpc_SecureHash";
    private static final String VPC_SECURE_HASH_TYPE_FIELD_NAME = "vpc_SecureHashType";
    private static final String VPC_VERSION_FIELD_NAME = "vpc_Version";
    private static final String VPC_MESSAGE_FIELD_NAME = "vpc_Message";
    private static final String VPC_RECEIPT_NO_FIELD_NAME = "vpc_ReceiptNo";
    private static final String VPC_TXN_RESPONSE_CODE_FIELD_NAME = "vpc_TxnResponseCode";
    private static final String USER_REFRESH_TOKEN_FIELD_NAME = "user_RefreshToken";

    @Value("${service-portal.payment.vpc.access-code}")
    private String accessCode;

    @Value("${service-portal.payment.vpc.version}")
    private String version;

    @Value("${service-portal.payment.vpc.command}")
    private String command;

    @Value("${service-portal.payment.vpc.merchant}")
    private String merchant;

    @Value("${service-portal.payment.vpc.locale}")
    private String locale;

    @Value("${service-portal.payment.vpc.return-url}")
    private String returnURL;

    @Value("${service-portal.payment.vpc.secure-hash-type}")
    private String secureHashType;

    @Value("${service-portal.payment.vpc.secure-secret}")
    private String secureSecret;

    @Value("${service-portal.payment.vpc.url}")
    private String vpcURL;

    private String shaHashAllFields(Map<String, String> fieldValuePairs, boolean sort)
            throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        List<String> fieldNames = new ArrayList<>(fieldValuePairs.keySet());
        if(sort) {
            Collections.sort(fieldNames);
        }
        StringBuilder stringBuilder = new StringBuilder();

        Iterator<String> iterator = fieldNames.iterator();
        while (iterator.hasNext()) {
            String fieldName = iterator.next();
            stringBuilder.append(fieldName).append("=").append(fieldValuePairs.get(fieldName));
            if (iterator.hasNext()) {
                stringBuilder.append('&');
            }
        }

        SecretKey secretKey = new SecretKeySpec(DatatypeConverter.parseHexBinary(this.secureSecret), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secretKey);
        mac.update(stringBuilder.toString().getBytes("ISO-8859-1"));
        return DatatypeConverter.printHexBinary(mac.doFinal());
    }

    public String getVpcURL(PaymentRequest paymentRequest, String refreshToken)
            throws NoSuchAlgorithmException, InvalidKeyException,
            UnsupportedEncodingException {

        Map<String, String> fieldValuePairs = new HashMap<>();
        fieldValuePairs.put(VPC_ACCESS_CODE_FIELD_NAME, this.accessCode);
        fieldValuePairs.put(VPC_AMOUNT_FIELD_NAME, Integer.toString(new Double(paymentRequest.getAmount()).intValue()));
        fieldValuePairs.put(VPC_COMMAND_FIELD_NAME, this.command);
        fieldValuePairs.put(VPC_LOCALE_FIELD_NAME, this.locale);
        fieldValuePairs.put(VPC_MERCHANT_FIELD_NAME, this.merchant);
        fieldValuePairs.put(VPC_MERCHTXREF_FIELD_NAME,
                paymentRequest.getUuid() + "/" + (paymentRequest.getNumberOfPaymentTries() + 1));
        fieldValuePairs.put(VPC_ORDER_INFO_FIELD_NAME, Long.toString(new Date().getTime()));
        fieldValuePairs.put(VPC_RETURN_URL_FIELD_NAME, this.returnURL);
        fieldValuePairs.put(VPC_VERSION_FIELD_NAME, this.version);
        fieldValuePairs.put(USER_REFRESH_TOKEN_FIELD_NAME, refreshToken);
        String secureHash = this.shaHashAllFields(fieldValuePairs, true);
        fieldValuePairs.put(VPC_SECURE_HASH_FIELD_NAME, secureHash);
        fieldValuePairs.put(VPC_SECURE_HASH_TYPE_FIELD_NAME, this.secureHashType);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        for (Map.Entry<String, String> entry : fieldValuePairs.entrySet()) {
            params.add(entry.getKey(), entry.getValue());
        }

        return UriComponentsBuilder.fromHttpUrl(this.vpcURL)
                .queryParams(params)
                .build()
                .encode()
                .toUriString();
    }

    public ReducedTransactionResponse reduceTransactionResponse(Map<String, String> allParams)
            throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        String secureHash = allParams.remove(VPC_SECURE_HASH_FIELD_NAME);
        allParams.remove(VPC_SECURE_HASH_TYPE_FIELD_NAME);
        ReducedTransactionResponse reducedTransactionResponse = new ReducedTransactionResponse();
        if(secureHash != null) {
            reducedTransactionResponse.setResponseValid(secureHash.equals(this.shaHashAllFields(allParams, false)));
        }
        reducedTransactionResponse.setAmount(allParams.get(VPC_AMOUNT_FIELD_NAME));
        reducedTransactionResponse.setMerchTxnRef(allParams.get(VPC_MERCHTXREF_FIELD_NAME));
        reducedTransactionResponse.setMessage(allParams.get(VPC_MESSAGE_FIELD_NAME));
        reducedTransactionResponse.setReceiptNo(allParams.get(VPC_RECEIPT_NO_FIELD_NAME));
        reducedTransactionResponse
                .setTxnResponseCodeDescription(getResponseDescription(allParams.get(VPC_TXN_RESPONSE_CODE_FIELD_NAME)));
        reducedTransactionResponse.setSuccessful(allParams.get(VPC_TXN_RESPONSE_CODE_FIELD_NAME).equals("0"));
        reducedTransactionResponse.setOrderInfo(allParams.get(VPC_ORDER_INFO_FIELD_NAME));
        return reducedTransactionResponse;
    }

    private String getResponseDescription(String vResponseCode) {

        String result;

        // check if a single digit response code
        if (vResponseCode.length() == 1) {

            // Java cannot switch on a string so turn everything to a char
            char input = vResponseCode.charAt(0);

            switch (input){
                case '0': result = "Transaction Successful"; break;
                case '1': result = "Transaction Declined"; break;
                case '2': result = "Bank Declined Transaction"; break;
                case '3': result = "No Reply from Bank"; break;
                case '4': result = "Expired Card"; break;
                case '5': result = "Insufficient Funds"; break;
                case '6': result = "Error Communicating with Bank"; break;
                case '7': result = "Payment Server detected an error"; break;
                case '8': result = "Transaction Type Not Supported"; break;
                case '9': result = "Bank declined transaction (Do not contact Bank)"; break;
                case 'A': result = "Transaction Aborted"; break;
                case 'B': result = "Transaction Declined - Contact the Bank"; break;
                case 'C': result = "Transaction Cancelled"; break;
                case 'D': result = "Deferred transaction has been received and is awaiting processing"; break;
                case 'E': result = "Transaction Declined - Refer to card issuer"; break;
                case 'F': result = "3-D Secure Authentication failed"; break;
                case 'I': result = "Card Security Code verification failed"; break;
                case 'L': result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
                case 'M': result = "Transaction Submitted (No response from acquirer)"; break;
                case 'N': result = "Cardholder is not enrolled in Authentication scheme"; break;
                case 'P': result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
                case 'R': result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
                case 'S': result = "Duplicate SessionID"; break;
                case 'T': result = "Address Verification Failed"; break;
                case 'U': result = "Card Security Code Failed"; break;
                case 'V': result = "Address Verification and Card Security Code Failed"; break;
                case '?': result = "Transaction status is unknown"; break;
                default: result = "Unable to be determined"; break;
            }

            return result;
        } else {
            return "No Value Returned";
        }
    }

    public static class ReducedTransactionResponse {

        private String merchTxnRef;
        private String amount;
        private String message;
        private String txnResponseCodeDescription;
        private String receiptNo;
        private boolean isResponseValid;
        private boolean isSuccessful;
        private String orderInfo;
//
//        public ReducedTransactionResponse(String merchTxnRef, String amount,
//                                          String message, String txnResponseCodeDescription,
//                                          String receiptNo, boolean isResponseValid) {
//            this.merchTxnRef = merchTxnRef;
//            this.amount = amount;
//            this.message = message;
//            this.txnResponseCodeDescription = txnResponseCodeDescription;
//            this.receiptNo = receiptNo;
//        }

        public ReducedTransactionResponse(){}

        public String getMerchTxnRef() {
            return merchTxnRef;
        }

        public void setMerchTxnRef(String merchTxnRef) {
            this.merchTxnRef = merchTxnRef;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getTxnResponseCodeDescription() {
            return txnResponseCodeDescription;
        }

        public void setTxnResponseCodeDescription(String txnResponseCodeDescription) {
            this.txnResponseCodeDescription = txnResponseCodeDescription;
        }

        public String getReceiptNo() {
            return receiptNo;
        }

        public void setReceiptNo(String receiptNo) {
            this.receiptNo = receiptNo;
        }

        public boolean isResponseValid() {
            return isResponseValid;
        }

        public void setResponseValid(boolean responseValid) {
            isResponseValid = responseValid;
        }

        public boolean isSuccessful() {
            return isSuccessful;
        }

        public void setSuccessful(boolean successful) {
            isSuccessful = successful;
        }

        public String getOrderInfo() {
            return orderInfo;
        }

        public void setOrderInfo(String orderInfo) {
            this.orderInfo = orderInfo;
        }
    }
}
