
# Build and Run
```java
./gradlew clean bootRun
```
# Usage
### Checking app is deployed sucessfullly
```sh
curl -i http://localhost:8080/hello
Unsecured Hello!
```
### Access secure resource with token
```sh
curl -i http://localhost:8080/secure

{
    "timestamp":1497554512525,
    "status":401,
    "error":"Unauthorized",
    "message":"Access Denied",
    "path":"/secure"
}
```

### Fetching refresh_token
```sh
curl -X POST -vu clientId:secret 'http://localhost:8080/oauth/token?username=<username>&password=<password>&grant_type=password'

{
    "access_token":"056c0d0d-a489-4fda-a8a6-0332df91a0f1",
    "token_type":"bearer",
    "refresh_token":"2334ffbb-e960-4b6e-b757-a8fc3df0eab8",
    "expires_in":1799,
    "scope":"read write"
}
```

### Fetching acess_token by submitting refresh_token
```sh
curl -X POST -vu clientId:secret 'http://localhost:8080/oauth/token?grant_type=refresh_token&refresh_token=<refresh_token>'

{
    "access_token":"821c99d4-2c9f-4990-b68d-18eacaff54b2",
    "token_type":"bearer",
    "refresh_token":"e6f8624f-213d-4343-a971-980e83f734be",
    "expires_in":1799,
    "scope":"read write"
}
```

### Access secure resource sucessfully
```sh
curl  -i -H "Authorization: Bearer <access_token>" http://localhost:8080/secure

Secure Hello!
```